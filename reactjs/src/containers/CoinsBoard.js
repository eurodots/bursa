import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {Link} from 'react-router-dom';


import Board from '../components/Dashboard/Board'

const styles = theme => ({
});

class CoinsBoard extends Component {

    render() {
        return (
          <div>
              <Board />
          </div>
        )
    }
}

CoinsBoard.propTypes = {
    styles: PropTypes.object
};

export default CoinsBoard
