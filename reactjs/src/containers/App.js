import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cssModules from 'react-css-modules';
import styles from '../style/index.scss';

import NavWrapper from '../components/NavWrapper'
import Preloader from '../components/Preloader'
import WindowListener from '../components/WindowListener'



// import { withStyles } from 'material-ui/styles';

import {withRouter} from 'react-router';
import {connect} from 'react-redux';

import {getCoinsArr} from '../utils/functions'

// import { connect } from 'react-redux';
// import * as ConfigActions from '../actions/config';
// import wrapActionCreators from '../utils/wrapActionCreators';

// @connect(state => ({
//     config: state.config
// }), wrapActionCreators(ConfigActions))

@cssModules(styles)
class App extends Component {


  constructor(props) {
    super(props)

    this.state = {
      data_loaded: false,
    }
  }

  componentWillMount() {

      this.preloadData()

  }

  preloadData() {

      const that = this
      getCoinsArr().then(function(response) {

          if(response) {
              that.props.onPreloadCoins(response.data.response)
          }

      })
  }


  renderWrapper() {
      const { children, styles } = this.props;

      // let config = this.props.config

      // {React.cloneElement(this.props.children, this.props)}
      return (
        <div className={styles.container}>
            <NavWrapper {...this.props}>

                {children}

                <WindowListener onWindowResize={this.props.onWindowResize} windowSize={this.props.config.windowSize} />

            </NavWrapper>
        </div>
      );
  }


  render() {

      // let showLayer = false
      // if(this.props.config) {
      //     showLayer = true
      // }
      // {showLayer ? this.renderWrapper() : <Preloader />}

      return (
          <div>

              {this.renderWrapper()}

          </div>
      )
  }
}


// static propTypes = {
//   children: PropTypes.any.isRequired,
//   styles: PropTypes.object
// };
App.propTypes = {
    children: PropTypes.any.isRequired,
    styles: PropTypes.object
};

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onToggleLeftMenu: (payload) => {
				dispatch({type: 'LEFT_MENU_TOGGLE', payload})
			},
			onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
            onPreloadCoins: (payload) => {
                dispatch({type: 'PRELOAD_COINS', payload})
            },
            onSelectCoins: (payload) => {
				dispatch({type: 'SELECT_COINS', payload})
			},
            onWindowResize: (payload) => {
				dispatch({type: 'WINDOW_RESIZE', payload})
			},
		})
	))
	(App)
);
