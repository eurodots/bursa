import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {Link} from 'react-router-dom';

import {willSellInfo} from '../utils/getWeb3.js'

const styles = theme => ({
});

class Test extends Component {

    checkToken() {
        let check = willSellInfo('0xafe5a978c593fe440d0c5e4854c5bd8511e770a4')
        console.log('check')
        console.log(check)
    }
    render() {
        return (
          <div onClick={() => this.checkToken()}>
              Test
          </div>
        )
    }
}

Test.propTypes = {
    styles: PropTypes.object
};

export default Test
