import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CounterPage from '../containers/CounterPage';
import AnotherPage from '../containers/AnotherPage';
import NotFoundPage from '../containers/NotFoundPage';

import AdminComponent from '../containers/Auth/Admin'
import ProtectedComponent from '../containers/Auth/Protected'
import LoginComponent from '../containers/Auth/Login'
import Coins from '../containers/Coins'
import CoinsBoard from '../containers/CoinsBoard'
import Test from '../containers/Test'




import { userIsAuthenticatedRedir, userIsNotAuthenticatedRedir, userIsAdminRedir,
         userIsAuthenticated, userIsNotAuthenticated } from '../auth'


// Need to apply the hocs here to avoid applying them inside the render method
const Login = userIsNotAuthenticatedRedir(LoginComponent)
const Protected = userIsAuthenticatedRedir(ProtectedComponent)
const Admin = userIsAuthenticatedRedir(userIsAdminRedir(AdminComponent))

export default (
  <Switch>
    <Route exact path="/" component={Coins}/>
    <Route exact path="/another" component={AnotherPage} />
    <Route exact path="/counter" component={CounterPage} />

    <Route exact path="/coins" component={Coins} />
    <Route path="/coins/:coin_id" component={CoinsBoard} />

    <Route exact path="/test" component={Test} />


    <Route path="/login" component={Login}/>
    <Route path="/protected" component={Protected}/>
    <Route path="/admin" component={Admin}/>

    <Route component={NotFoundPage} />


  </Switch>
);
