// console.log( process.env )

export const ENV = process.env.NODE_ENV || 'development';
export const isProduction = ENV === 'production';

export const apiDomain = isProduction ? 'https://bursadex.com' : 'http://bursadex.com:8888';
// export const apiDomain = isProduction ? 'http://bursadex.com:8888' : 'http://bursadex.com:8888';
export const apiEndpoint = `${apiDomain}/api/v1`

export const apiEtherscan = 'VJT69AJD19KQDUBKSJKAEBEQRA21WJBIPC'

export const confProjectName = 'BursaDex.com'
export const confProjectVersion = 'beta 1.0'
