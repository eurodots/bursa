import React, {Component} from 'react';
// import PropTypes from 'prop-types';


class WindowListener extends Component {

    constructor(props) {
        super(props);
        this.state = {}
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    timeout = false
    clearTimer = () => {
        var that = this
        clearTimeout(that.timeout)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
        this.clearTimer()
    }
    updateWindowDimensions() {
        this.props.onWindowResize([window.innerWidth, window.innerHeight])
    }
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }


    render() {

        let windowSize = this.props.windowSize

        return <div />
        return (
            <div>
                Windows listener<br />
                {windowSize[0]}<br />
                {windowSize[1]}<br />
            </div>
        )
    }
}

export default WindowListener
