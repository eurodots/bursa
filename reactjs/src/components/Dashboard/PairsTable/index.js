import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom'


import Preloader from '../../Preloader'
import Icon from 'material-ui/Icon';
import Button from 'material-ui/Button';
import NumberFormat from 'react-number-format';
import CountUp from 'react-countup';

import Details from '../Details'

import theme from './theme.scss'

const _ = require('lodash');


// import React from 'react';
import classNames from 'classnames';
// import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, {
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from 'material-ui/Table';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import { lighten } from 'material-ui/styles/colorManipulator';

import Input, { InputLabel, InputAdornment } from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';




const Counter = (props) => {
    return (
        <span>
            <CountUp
                start={0}
                end={props.value}
                duration={10}
                useEasing={true}
                useGrouping={true}
                separator=" "
                decimals={2}
                decimal="."
                style={{margin: '0 3px 0 0'}}
                />
        </span>
    )
}

const PriceChanges = (props) => {

    let type = true
    let value = props.value
    if(value < 0) {
        value = Math.abs(value)
        type = false
    }

    return (
        <div>
            {type
              ?
                  <div data-color="success">
                      <Icon>arrow_drop_up</Icon>
                      <Counter value={value} /> %
                  </div>
              :
                  <div data-color="danger">
                      <Icon>arrow_drop_down</Icon>
                      <Counter value={value} /> %
                  </div>
              }
        </div>
    )
}


class InputSearch extends React.Component {
    state = {
        search_value: '',
    }

    handleChange = name => event => {
        this.setState({
          [name]: event.target.value,
        });

        this.props.searchValue(event.target.value)
    };

    render() {
        // <FormControl className={classes.formControl}>
        return (
            <FormControl data-el="search">
                  <InputLabel htmlFor="password">Search</InputLabel>
                  <Input
                    id="table-search"
                    type="text"
                    value={this.state.search_value}
                    onChange={this.handleChange('search_value')}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton>
                          <Icon>search</Icon>
                        </IconButton>
                      </InputAdornment>
                    }
                  />
            </FormControl>
        )
    }
}


const columnData = [
  { id: 'name', numeric: false, disablePadding: true, label: 'Name' },
  { id: 'market', numeric: true, disablePadding: true, label: 'Market' },
  { id: 'price', numeric: true, disablePadding: true, label: 'Price' },
  { id: 'volume24', numeric: true, disablePadding: true, label: 'Volume (24h)' },
  { id: 'supply', numeric: true, disablePadding: true, label: 'Circulating Supply' },
  { id: 'change24', numeric: true, disablePadding: true, label: 'Change (24h)' },
  { id: 'actions', numeric: false, disablePadding: true, label: 'Actions' },
  // { id: 'protein', numeric: true, disablePadding: false, label: 'Protein (g)' },
];

class EnhancedTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount } = this.props;

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            />
          </TableCell>
          {columnData.map(column => {
            return (
              <TableCell
                key={column.id}
                numeric={column.numeric}
                padding={column.disablePadding ? 'none' : 'default'}
                sortDirection={orderBy === column.id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === column.id}
                    direction={order}
                    onClick={this.createSortHandler(column.id)}
                  >
                    {column.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            );
          }, this)}
        </TableRow>
      </TableHead>
    );
  }
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const toolbarStyles = theme => ({
  root: {
    paddingRight: theme.spacing.unit,
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.dark,
          backgroundColor: lighten(theme.palette.secondary.light, 0.4),
        }
      : {
          color: lighten(theme.palette.secondary.light, 0.4),
          backgroundColor: theme.palette.secondary.dark,
        },
  spacer: {
    flex: '1 1 100%',
  },
  actions: {
    color: theme.palette.text.secondary,
  },
  title: {
    flex: '0 0 auto',
  },
});

let EnhancedTableToolbar = props => {
  const { numSelected, classes } = props;

  return (
    <Toolbar className={classes.root}>
      <div className={classes.title}>
        <Typography variant="title">Coins list</Typography>
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
          <div className={theme.tableFilters}>
              <InputSearch searchValue={props.searchValue} />

              <Tooltip title="Filter list">
                <IconButton aria-label="Filter list">
                  <Icon>filter_list_icon</Icon>
                </IconButton>
              </Tooltip>
          </div>
      </div>
    </Toolbar>
  );
};

EnhancedTableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  searchValue: PropTypes.func.isRequired,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
  root: {
    width: '100%',
    // marginTop: theme.spacing.unit * 3,
  },
  table: {
    // minWidth: 800,
    width: '100%',
  },
  tableWrapper: {
    // overflowX: 'auto',
  },
});

class PairsTable extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      order: 'asc',
      orderBy: 'name',
      selected: [],
      data: [],
      data_original: [],
      page: 0,
      rowsPerPage: 25,
    };
  }

    componentDidMount () {
        this.preloadData()
    }


    preloadData() {
        const config = this.props.config
        const that = this

        if(config.coins_arr.length == 0) {
            setTimeout(function() {
                that.preloadData()
            }, 100)
        } else {
            const data = _.values(config.coins_data)
            // console.log('data')
            // console.log(data)
            this.setState({
                data: data,
                data_original: data,
                selected: config.selected_coins,
            })
        }
    }

    openDetails = (event, coin) => {
        event.stopPropagation();
        event.preventDefault();
		this.details.loadDetails(coin)
        // alert(coin)
	}

    tradeNow(event, coin) {
        event.stopPropagation();
        event.preventDefault();
        this.props.history.push(`/coins/${coin}`)
    }



    // loadSelectedCoins() {
    //     this.setState({selected: this.props.config.selected_coins})
    // }

    searchValue = (value) => {
        const {data} = this.state

        let searchValue = value.toLowerCase().replace(/ /g,'')

        if(searchValue.length > 0) {

            let coins_arr = {}
            data.map((item, index) => {
                if(item.coin.toLowerCase().indexOf(searchValue) !== -1 || item.label.toLowerCase().indexOf(searchValue) !== -1) {
                    coins_arr[item.coin] = item
                }
            })

            this.setState({data: _.values(coins_arr)})
        } else {
            this.setState({data: this.state.data_original})
        }

    }


    showAllResults = () => {
        this.setState({data: this.state.data_original})
    }

    tableSearchHelper() {
        const {data, data_original} = this.state

        let dataDifference = 0
        let original_original = data_original.length
        let new_length = data.length
        if(new_length < original_original) {
            dataDifference = original_original - new_length
        }

        if(dataDifference != 0) {
            return (
                <div className={theme.tableSearchHelper}>
                    <div><strong>Found:</strong> {new_length} from {original_original} coins</div>
                    <Button color="primary" onClick={this.showAllResults}>Show other {dataDifference}</Button>
                </div>
            )
        } else {
            return ''
        }
    }


  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    const data =
      order === 'desc'
        ? this.state.data.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : this.state.data.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));

    this.setState({ data, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.state.data.map(n => n.id) });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
      this.props.onSelectCoins({type: false, array: [id]})
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected }, function() {
        this.props.onSelectCoins({type: true, array: newSelected})
    });


  };


  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;


  renderTable() {
      const { classes } = this.props;
      const { data, order, orderBy, selected, rowsPerPage, page } = this.state;
      const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

      // console.log(this.state.data)

      if(data.length == 0) {
          return <Preloader />
      }

      return (
        <Paper className={classes.root}>
          <EnhancedTableToolbar numSelected={selected.length} searchValue={this.searchValue} />
          {this.tableSearchHelper()}
          <div className={classes.tableWrapper}>
            <Table className={theme.coinTable}>
              <EnhancedTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={data.length}
              />
              <TableBody>
                {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(n => {
                  const isSelected = this.isSelected(n.coin);

                  return (
                    <TableRow
                      hover
                      onClick={event => this.handleClick(event, n.coin)}
                      role="checkbox"
                      aria-checked={isSelected}
                      tabIndex={-1}
                      key={n.coin}
                      selected={isSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox checked={isSelected} />
                      </TableCell>
                      <TableCell padding="none">
                          <ul data-el="name" onClick={(event) => this.openDetails(event, n.coin)}>
                              <li><img src={n.favicon32} /></li>
                              <li>
                                  <label>{n.coin}</label>
                                  <span>{`(${n.label})`}</span>
                              </li>
                          </ul>
                      </TableCell>
                      <TableCell numeric>
                          <NumberFormat data-el="numberFormat" value={n.history.capital_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='$ ' />
                      </TableCell>
                      <TableCell numeric>
                          <NumberFormat data-el="numberFormat" value={n.history.price_usd} displayType={'text'} decimalScale={5} thousandSeparator={true} prefix='$ ' />
                      </TableCell>
                      <TableCell numeric>
                          <NumberFormat data-el="numberFormat" value={n.history.volume24_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='$ ' />
                      </TableCell>
                      <TableCell numeric>
                          <NumberFormat data-el="numberFormat" value={n.history.suply_number} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix="" />
                          <strong style={{marginLeft: 7}}><sup>{n.coin}</sup></strong>
                      </TableCell>
                      <TableCell numeric>
                          <PriceChanges value={n.history.changes_24h} />
                      </TableCell>

                      <TableCell>
                          <div data-el="buttons">
                              <Button variant="raised" color="primary" onClick={(event) => this.tradeNow(event, n.contract_address)}>Trade</Button>
                          </div>
                      </TableCell>


                    </TableRow>
                  );
                })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    colSpan={6}
                    count={data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                      'aria-label': 'Previous Page',
                    }}
                    nextIconButtonProps={{
                      'aria-label': 'Next Page',
                    }}
                    onChangePage={this.handleChangePage}
                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </div>
        </Paper>
      );
  }
  render() {
      return (
          <div>

              <Details onRef={ref => (this.details = ref)} />

              {this.renderTable()}
          </div>
      )
  }
}

PairsTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(PairsTable));
