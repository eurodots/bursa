// @flow weak

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import Button from 'material-ui/Button';

import theme from './theme.scss'

const styles = theme => ({
});

class ContractBar extends React.Component {


    render() {
        const {classes} = this.props;

        const {data, tokenAddress} = this.props
        // console.log(config)

        return (
            <ul className={theme.dataList}>
                <li>
                    <span data-el="label">{data.label} contract:</span>
                    <span data-el="contractAddress" onClick={() => this.props.openDetails(data.symbol)}>
                        {tokenAddress}
                    </span>
                </li>
                <li>
                    <Button color="primary" onClick={() => this.props.openDetails(data.coin)}>Details</Button>
                </li>
            </ul>
        )
    }
}

ContractBar.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ContractBar);
