import React from 'react';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';


import TimeAgo from 'react-timeago';
import Preloader from '../../Preloader'

// import MetaMaskLogo from './MetaMaskLogo'
// import Pairs from './Pairs/index.js'
import {getCurrentToken} from '../../../utils/functions'


import Details from '../Details'
import HeikinAshi from '../../HeikinAshi'
import AskBidForm from './AskBidForm'
import OrdersBook from './OrdersBook'
import ContractBar from './ContractBar'
import BalanceApprove from './BalanceApprove'

import theme from './theme.scss'

const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

class Dashboard extends React.Component {

	constructor (props) {
		super(props)
		this.state = {
			data: false,
            MetaMaskLoaded: false,
		}
		this.openDetails = this.openDetails.bind(this)
        // this.returnDataFromOrdersBook = this.returnDataFromOrdersBook.bind(this)
	}

	openDetails(coin) {
		this.refDetails.loadDetails(coin)
	}




    // componentWillMount() {
        // const coin = this.props.match.params.coin_id
        // this.props.onSelectCoins({type: true, array: [coin]})
        // console.log(coin)
        // this.timeout = setTimeout(() => {
        //     this.setState({MetaMaskLoaded: true})
        // }, 3000)
    // }

	componentDidMount() {

        this.loadData(this.props.match.params.coin_id)
        // this.getBalanceApproved()
		// this.props.onHideAllMenus()

        console.log('<Dashboard /> componentDidMount()')
	}

	componentWillReceiveProps(nextProps) {

		const coin_next = nextProps.match.params.coin_id
		const coin_this = this.props.match.params.coin_id
		if(coin_next != coin_this) {
			this.setState({data: false})
			this.loadData(nextProps.match.params.coin_id)
		}
	}

	loadData(coin) {
		// const coin = this.props.match.params.coin_id
		const that = this
		getCurrentToken(coin).then(function(response) {
            if(response) {
                console.log(response.data)
                that.setState({data: response.data}, function() {
                    document.title = response.data.name
                })
            }
		})
	}

    // returnDataFromOrdersBook(type) {
    //     return this.refOrdersBook.returnDataToRef(type)
    // }




	renderBoard() {
		const { message } = this.props
		const {data} = this.state

        const token_address = this.props.match.params.coin_id
        const user_address= this.props.config.web3Metamask.user_address

        if(!user_address) {
            return <Preloader />
        }

		return (
			<div>
				<Details onRef={ref => (this.refDetails = ref)} />

				<ul className={theme.boardHeader}>
					<li>
						<img src={data.favicon32} />
					</li>
					<li>
						<Typography variant="display1" className={theme.title}>
							{data.symbol}
							<span>_ETH</span>
						</Typography>
                        <div onClick={this.fetchDataFromOrdersBook}>Check</div>
					</li>
				</ul>

				<div className={theme.wrapper}>
                    <div className={theme.container} data-el="header">
                        <div className={theme.content}>
                            {data ? <ContractBar data={data} tokenAddress={token_address} openDetails={this.openDetails} /> : ''}
                        </div>
                        <div className={theme.sidebar}>
                            <BalanceApprove
                                onRef={ref => (this.refBalanceApprove = ref)}
                                tokenAddress={token_address}
                                userAddress={user_address}
                                />
                        </div>
                    </div>
                    <div className={theme.container}>
                        <div className={theme.sidebar}>
							<OrdersBook
                                tokenAddress={token_address}
                                userAddress={user_address} />
                        </div>
                        <div className={theme.content}>
                            <HeikinAshi tokenAddress={token_address} />
                        </div>
                        <div className={theme.sidebar}>
                            <AskBidForm
                                tokenAddress={token_address}
                                userAddress={user_address}
                                refBalanceApprove={this.refBalanceApprove}
                                coinFrom={data.symbol ? data.symbol : '???'} />
                        </div>
                    </div>
                </div>

                {this.renderTransactions()}

			</div>
		);
	}

	renderBoardMobile() {
		const {classes} = this.props

		return (
			<div className={classes.root}>
				<ExpansionPanel>
					<ExpansionPanelSummary expandIcon={<Icon>expand_more_icon</Icon>}>
						<Typography className={classes.heading}>Orders Book</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails>
						orders
					</ExpansionPanelDetails>
				</ExpansionPanel>
				<ExpansionPanel>
					<ExpansionPanelSummary expandIcon={<Icon>expand_more_icon</Icon>}>
						<Typography className={classes.heading}>Pairs Book</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails>
						pairs????
					</ExpansionPanelDetails>
				</ExpansionPanel>
				<ExpansionPanel defaultExpanded={false}>
					<ExpansionPanelSummary expandIcon={<Icon>expand_more_icon</Icon>}>
						<Typography className={classes.heading}>Chart</Typography>
					</ExpansionPanelSummary>
					<ExpansionPanelDetails>
						chart
					</ExpansionPanelDetails>
				</ExpansionPanel>
			</div>
		)
	}

    renderTransactions() {

        const {data} = this.state

        let user_address = this.props.config.web3Metamask.user_address
        let transactions = this.props.config.web3Transactions
            transactions = transactions[user_address] ? transactions[user_address] : []

        return (
            <div className={theme.blockTransactions}>

                <Typography variant="headline" gutterBottom>
                    Your transactions for {this.state.data.coin}
                </Typography>

                <ul>
                    <li>
                        Wallet: {this.props.config.web3Metamask.user_address}
                    </li>
                </ul>

                <div className={theme.transactionsList}>
                    {transactions.map((item, index) => {
                        if(item.coin == data.coin) {
                            let hash_url = `https://etherscan.io/tx/${item.hash}`
                            return (
                                <ul key={index}>
                                    <li data-el="coin">{item.coin}</li>
                                    <li data-el="value">{item.value} ETH</li>
                                    <li data-el="hash">
                                        <a href={hash_url} target="_blank">{item.hash}</a>
                                    </li>
                                    <li data-el="date">
                                        <TimeAgo date={item.date} /> ago
                                    </li>
                                </ul>
                            )
                        }
                    })}
                </div>
            </div>
        )
    }

	render() {

		const {data, MetaMaskLoaded} = this.state
		const windowSize = this.props.config.windowSize
		// console.log(this.props)

        return (
            <div>
                {this.renderBoard()}
            </div>
        )
        return (
			<div>
				{data
				?
					windowSize[0] > 940 ? this.renderBoard() : this.renderBoardMobile()
				:
					<Preloader />
				}
			</div>
		)
	}
}

Dashboard.propTypes = {
	classes: PropTypes.object.isRequired
};


export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onSelectCoins: (payload) => {
				dispatch({type: 'SELECT_COINS', payload})
			},
            onHideAllMenus: (payload) => {
				dispatch({type: 'HIDE_ALL_MENUS', payload})
			},
		})
	))
	(withStyles(styles)(Dashboard))
);
