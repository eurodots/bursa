// @flow weak

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
// import {CircularProgress} from 'material-ui/Progress';
// import purple from 'material-ui/colors/purple';
import Typography from 'material-ui/Typography';
// import Divider from 'material-ui/Divider';
import Tooltip from 'material-ui/Tooltip';


import Blockies from 'react-blockies';

const _ = require('lodash');

import theme from './theme.scss'

const styles = theme => ({
    root: {
        // padding: '10px 20px 20px',
    }
});


class OrdersBook extends React.Component {

    state = {
        // data_orders: [],
    }

    // componentDidMount() {
    //     this.applyData(this.props.dataOrders)
    // }
    //
    // componentWillReceiveProps(nextProps) {
    //     console.log('nextProps')
    //     console.log(nextProps)
    //     this.applyData(nextProps.dataOrders)
    //
    // }
    // applyData(data) {
    //     this.setState({data_orders: data})
    // }


    renderOrdersList(type) {

        let data_orders = this.props.dataOrders

        if(type == 'buy') {
            data_orders = _.orderBy(data_orders, ['wei_price'], ['asc']);
        }

        let totalVolume = 0
        let totalCounter = 0
        data_orders.map((item, index) => {
            if(item.type == type) {
                totalVolume += parseFloat(item.amount)
                totalCounter++
            }
        })
        // totalVolume = 50

        return (
            <div className={theme.ordersListWrapper}>


                <div data-el={type}>
                    <ul data-el="title">
                        <li>
                            <Typography variant="button">
                                {type} ({totalCounter})
                            </Typography>
                        </li>
                        <li>
                            Total amount: {totalVolume}
                        </li>
                    </ul>
                    <ul data-el="list">
                        <li>
                            <ul data-el="rowHeader">
                                <li>#</li>
                                <li></li>
                                <li>Price</li>
                                <li>Amount</li>
                                <li>Total</li>
                            </ul>
                        </li>
                        {data_orders.map((item, index) => {
                            if(item.type == type) {

                                let volumeBar = (100 / totalVolume) * parseFloat(item.amount)
                                    volumeBar = volumeBar.toFixed(0)
                                    volumeBar = `${volumeBar}%`
                                    // volumeBar = Number(item.amount)/1000000000000000000
                                 // volumeBar = item.amount
                                return (
                                    <li key={index}>
                                        <div data-el="volumeBar">
                                            <div style={{width: volumeBar}}></div>
                                        </div>
                                        <ul data-el={this.props.userAddress == item.user_address ? 'rowOrderOwner' : 'rowOrder'}>
                                            <li data-el="id">
                                                {item.spot_id}
                                            </li>
                                            <li data-el="user">
                                                <Tooltip title={item.user_address}>
                                                    <div data-el="userIcon">
                                                        <Blockies
                                                            seed={item.user_address}
                                                            size={5}
                                                            scale={3}
                                                            color="#3f51b5"
                                                            bgColor="#fff"
                                                            spotColor="#FF4081"
                                                          />
                                                    </div>
                                                </Tooltip>
                                            </li>
                                            <li data-el="price">{item.price}</li>
                                            <li data-el="amount">{item.amount}</li>
                                            <li data-el="total">{item.total}</li>
                                        </ul>
                                    </li>
                                )
                            }
                        })}
                    </ul>
                </div>
            </div>
        )
    }
    render() {
        const {classes} = this.props;
        // const {data_orders} = this.state

        return (
            <div className={classes.root}>
                {this.renderOrdersList('sell')}
                {this.renderOrdersList('buy')}
            </div>
        )
    }
}

OrdersBook.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(OrdersBook);
