// @flow weak

import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import Typography from 'material-ui/Typography';
import Tooltip from 'material-ui/Tooltip';
import Icon from 'material-ui/Icon';


// import Blockies from 'react-blockies';
import {web3orderCancel} from '../../../../utils/getWeb3.js'

const _ = require('lodash');

import theme from './theme.scss'

const styles = theme => ({
    root: {
        // padding: '10px 20px 20px',
    }
});

class OrdersOwner extends React.Component {

    state = {
        // data_orders: [],
    }

    // componentDidMount() {
    //     this.setState({data_orders: this.props.dataOrders})
    // }

    cancelOrder(type, item) {

        let init = {
            type: type,
            // amount: item.amount,
            token: this.props.tokenAddress,
            price: item.wei_price,
            spot_id: item.spot_id
        }
        console.log(init)
        web3orderCancel(init)
        this.props.onToggleRightMenu('hideAccount')

        // console.log(init)

        // return false
    }

    renderOrdersList(type) {
        return (
            <ul data-el="ordersOwner">
                <li>
                    <ul data-el="rowHeader">
                        <li>Price</li>
                        <li>Amount</li>
                        <li>#</li>
                        <li></li>
                    </ul>
                </li>
                {this.props.dataOrders.map((item, index) => {
                    if(item.type == type) {
                        return (
                            <li key={index}>
                                <ul>
                                    <li data-el="price">{item.price}</li>
                                    <li data-el="amount">{item.amount}</li>
                                    <li data-el="spot">{item.spot_id}</li>
                                    <li>
                                        <Tooltip title="Cancel this order" placement="right">
                                            <Icon
                                                data-el="iconDelete"
                                                onClick={() => this.cancelOrder(type, item)}>close</Icon>
                                        </Tooltip>
                                    </li>
                                </ul>
                            </li>
                        )
                    }
                })}
            </ul>
        )
    }

    render() {
        const {classes} = this.props;
        // const {data_orders} = this.state

        return (
            <div className={classes.root}>

                <Typography variant="button">
                    SELL
                </Typography>
                {this.renderOrdersList('sell')}

                <Typography variant="button">
                    BUY
                </Typography>
                {this.renderOrdersList('buy')}

            </div>
        )
    }
}

OrdersOwner.propTypes = {
	classes: PropTypes.object.isRequired
};

// export default withStyles(styles)(OrdersOwner);

export default
withRouter(
	(connect(
		(mapStateToProps) => (mapStateToProps),
		dispatch => ({
			onToggleRightMenu: (payload) => {
				dispatch({type: 'RIGHT_MENU_TOGGLE', payload})
			},
		})
	))
	(withStyles(styles)(OrdersOwner))
);
