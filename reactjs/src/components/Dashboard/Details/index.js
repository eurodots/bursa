import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import { withRouter } from 'react-router-dom'
// import Button from 'material-ui/Button';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Typography from 'material-ui/Typography';

import Icon from 'material-ui/Icon';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';


import {getCurrentCoin} from '../../../utils/functions'

import {CopyToClipboard} from 'react-copy-to-clipboard';
import TimeAgo from 'react-timeago';
import { getTimeAgoFormat } from '../../../utils/timeAgo_formatter'
import CountUp from 'react-countup';
import NumberFormat from 'react-number-format';

import HeikinAshi from '../../HeikinAshi'

const _ = require('lodash');

import theme from './theme.scss'

const Counter = (props) => {
    return (
        <span>
            <CountUp
                start={0}
                end={props.value}
                duration={5}
                useEasing={true}
                useGrouping={true}
                separator=" "
                decimals={2}
                decimal="."
                style={{margin: '0 3px 0 0'}}
                />
        </span>
    )
}

const PriceChanges = (props) => {

    let type = true
    let value = props.value
    if(value < 0) {
        value = Math.abs(value)
        type = false
    }

    return (
        <div>
            <label>{props.label}</label>
            {type
              ?
                  <div data-color="success">
                      <Icon>arrow_drop_up</Icon>
                      <Counter value={value} /> %
                  </div>
              :
                  <div data-color="danger">
                      <Icon>arrow_drop_down</Icon>
                      <Counter value={value} /> %
                  </div>
              }
        </div>
    )
}


const styles = {
  listFull: {
    width: 'auto',
  },
};

class Details extends React.Component {
  state = {
    open: false,
    data: false,
    copied: false,
  };

  componentDidMount() {
    this.props.onRef(this)
  }
  componentWillUnmount() {
    this.props.onRef(undefined)
  }

  loadDetails(coin) {
      const that = this
      getCurrentCoin(coin).then(function(response) {
          that.setState({
              open: true,
              data: response.data.response.data,
              copied: false,
          })
      })
  }


  tradeNow = () => {
      const {data} = this.state

      this.setState({open: false})
      this.props.history.push(`/coins/${data.contract_address}`)

  }


  toggleDrawer = (open) => () => {
    this.setState({
        open: open,
        copied: false,
    })
  };


  renderLinks() {
      const {data} = this.state

      return (
          <div className={theme.blockLinks}>
              <ul>
                  <li>
                      <a href={data.coinmarketcap} target="_blank">Coinmarketcap</a>
                  </li>
                  {data.links.map((item, index) => {
                      return (
                          <li key={index}>
                              <a href={item.value} target="_blank">{item.label}</a>
                          </li>
                      )
                  })}
              </ul>
          </div>
      )
  }

  renderCapital() {
      const {data} = this.state

      // console.log(data)

      return (
          <ul className={theme.blockChanges}>
              <li>
                  <label>Market</label>
                  <NumberFormat data-el="numberFormat" value={data.history.capital_usd} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='$ ' />
              </li>
              <li>
                  <label>Price</label>
                  <NumberFormat data-el="numberFormat" value={data.history.price_usd} displayType={'text'} decimalScale={5} thousandSeparator={true} prefix='$ ' />
              </li>
              <li>
                  <label>Price</label>
                  <NumberFormat data-el="numberFormat" value={data.history.price_btc} displayType={'text'} decimalScale={5} thousandSeparator={true} prefix='BTC ' />
              </li>
              <li>
                  <label>Supply ({data.coin})</label>
                  <NumberFormat data-el="numberFormat" value={data.history.suply_number} displayType={'text'} decimalScale={0} thousandSeparator={true} prefix='' />
              </li>
          </ul>
      )
  }

  renderContract() {
      const {data, copied} = this.state

      return (
          <ul className={theme.blockContract}>
              <li>
                  <strong>Contract:</strong> {data.contract_address}
              </li>
              <li>

                  <Tooltip title="Copy" placement="bottom">
                    <CopyToClipboard text={data.contract_address}
                      onCopy={() => this.setState({copied: true})}>

                      {copied
                      ?
                          <IconButton aria-label="Copied">
                              <Icon>check</Icon>
                          </IconButton>
                      :
                          <IconButton aria-label="Copy" color="primary">
                              <Icon>content_copy</Icon>
                          </IconButton>
                      }
                    </CopyToClipboard>
                </Tooltip>

            </li>

          </ul>
      )
  }



  renderTitle() {
      const {data} = this.state


      return (
          <div className={theme.blockTitle}>
              <ul>
                  <li>
                        <Typography variant="headline" className={theme.title}>
                            <img src={data.favicon32} />
                            {data.coin}
                            <span data-el="short">
                                {`(${data.label})`}
                            </span>
                        </Typography>
                  </li>
                  <li>
                      <span data-el="rank">Rank {data.rank} from {data.rank_max}</span>
                  </li>
                  <li>
                      <Button variant="raised" color="primary" onClick={this.tradeNow}>Trade now!</Button>
                  </li>
              </ul>
          </div>
      )
  }

  renderChanges() {
      const {data} = this.state


      return (
          <ul className={theme.blockChanges}>
              <li>
                  <PriceChanges label="Changes 1h" value={data.history.changes_1h} />
              </li>
              <li>
                  <PriceChanges label="Changes 24h" value={data.history.changes_24h} />
              </li>
              <li>
                  <PriceChanges label="Changes 7d" value={data.history.changes_7d} />
              </li>
              <li className={theme.updatedAt}>
                  <label>Updated</label>
                  <div>
                      <TimeAgo date={data.updated_date} formatter={getTimeAgoFormat()} /> ago
                  </div>
              </li>
          </ul>
      )
  }

  renderChart() {

      const {data} = this.state
      if(!data.contract_address) {
          return (
              <div>
                  loading...
              </div>
          )
      }
      return (
          <div className={theme.blockChart}>
              <HeikinAshi tokenAddress={data.contract_address} />
          </div>
      )
  }

  renderAll() {
      const { classes } = this.props;
      const {data} = this.state
      // console.log(data)

      return (
          <div>
               {this.renderContract()}

                <div className={theme.blockWrapper}>
                    {this.renderTitle()}
                    {this.renderCapital()}
                    {this.renderChanges()}
                    {this.renderLinks()}
                    {this.renderChart()}
                </div>
        </div>
      );
  }

  render() {
      const {data} = this.state

      return (
          <Drawer anchor="right" open={this.state.open} onClose={this.toggleDrawer(false)}>
            <div
              tabIndex={0}
              role="button"
              className={theme.wrapper}
            >
                {data ? this.renderAll() : 'No data...'}
            </div>
          </Drawer>
      )
  }
}

Details.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(Details));
