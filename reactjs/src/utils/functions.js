import {apiDomain, apiEndpoint, apiEtherscan} from '../config/init'

// import etherscan from 'etherscan-api';
import axios from 'axios';
// import CircularJSON from 'circular-json';
const _ = require('lodash');







export function getEtherscan() {
	// api = etherscan.init(apiEtherscan);
	//
	// var balance = api.account.balance('0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae');
	// balance.then(function(balanceData){
	//   console.log(balanceData);
	// });
	//
	// return balance
}

export function getTokenData(token=false) { // FUNCTION NOT USED YET
	titleLoading(true)

	const url = `https://api.ethplorer.io/getAddressInfo/${token}?apiKey=freekey`
    console.log(url)
	// console.log('getTokenData()')

	const data = axios.get(url).then((res) => {
		// let json = CircularJSON.stringify(res);
		// json = CircularJSON.parse(json)
		return res.data;
	}).catch(err => { throw err });

	titleLoading(false)
	console.log(data)
	return data
}


export function getCoinsArr() {
	titleLoading(true)

	const url = `${apiEndpoint}/coins`
    console.log(url)
	// console.log('getCoinsArr()')

	const data = axios.get(url).then(function(response) {
		return response
	}).catch(function(error) {
		console.log(error);
	})

	titleLoading(false)
	return data
}


export function getCurrentToken(token='unnamed') {
	titleLoading(true)

	// const url = `${apiEndpoint}/coins/${token}`
	const url = `https://api.ethplorer.io/getTokenInfo/${token}?apiKey=freekey`
    console.log(url)
	// console.log('getCurrentCoin()')

	const data = axios.get(url).then(function(response) {
		return response
	}).catch(function(error) {
		console.log(error);
	})

	titleLoading(false)
	return data
}

export function getCurrentCoin(coin='unnamed') {
	titleLoading(true)

	const url = `${apiEndpoint}/coins/${coin}`
    console.log(url)
	// console.log('getCurrentCoin()')

	const data = axios.get(url).then(function(response) {
		return response
	}).catch(function(error) {
		console.log(error);
	})

	titleLoading(false)
	return data
}




// ANIMATED TITLE
let intervalInit = ''
function animatedTitle(writeText, interval, visibleLetters) {
    var _instance = {};

    var _currId = 0;
    var _numberOfLetters = writeText.length;

    function updateTitle() {
        _currId += 1;
        if(_currId > _numberOfLetters - 1) {
            _currId = 0;
        }

        var startId = _currId;
        var endId = startId + visibleLetters;
        var finalText;
        if(endId < _numberOfLetters - 1) {
            finalText = writeText.substring(startId, endId);
        } else {
            var cappedEndId = _numberOfLetters;
            endId = endId - cappedEndId;

            finalText = writeText.substring(startId, cappedEndId) +     writeText.substring(0, endId);
        }

        document.title = finalText;
    }

    _instance.init = function() {
        intervalInit = setInterval(updateTitle, interval);
    };
	_instance.stop = function() {
		clearInterval( intervalInit );
    };

    return _instance;
}

function titleLoading(type) {
	let title = new animatedTitle("Loading data... ", 300, 10);
	if(type) {
		title.init();
	} else {
		title.stop();
	}
}
