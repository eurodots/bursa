// import CircularJSON from 'circular-json';
import axios from 'axios';

import Web3 from 'web3';
import BursaContract from './contract.js';

let bursaInstance = false
if(typeof web3 !== 'undefined') {
    bursaInstance = web3.eth.contract(BursaContract.abi).at(BursaContract.address)
}



// INIT WEB3 WITH CONTRACT
function initWeb3() {
    let data = []

    window.addEventListener('load', function() {
        let web3 = window.web3

        // Checking if Web3 has been injected by the browser (Mist/MetaMask)
        if (typeof web3 !== 'undefined') {
          // Use Mist/MetaMask's provider.
          web3 = new Web3(web3.currentProvider)
          data.web3 = web3
          data.status = 'MetaMask activated!'
        } else {
          // Fallback to localhost if no web3 injection. We've configured this to
          // use the development console's port by default.
          web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"))
          data.web3 = web3
          data.status = 'No web3 instance injected, using Local web3.'
        }
    })

    return data
}



export async function getWeb3() {


    let response = await new Promise(function(resolve, reject) {

        // let web3_data = []
        let web3_data = initWeb3()
        let web3_user_address = false
        let web3_user_balance = false
        let web3_contract_balance = false

        // RETRIEVE DATA...

            // USER ADDRESS
            web3.eth.getAccounts((error, accounts) => {
                web3_user_address = accounts[0]


                // USER BALANCE
                if(web3_user_address) {
                    web3.eth.getBalance(web3_user_address, async function (err, res) {
                      if (!err) {
                          web3_user_balance = fromWei(res)


                          // USER BALANCE AR CONTRACT
                          // if(typeof(bursaInstance.balanceOf(web3_user_address, () => {})) !== 'function') {
                          //     return false
                          // }
                          await bursaInstance.balanceOf(web3_user_address, async function(err, res){
                              if(!err) {
                                  web3_contract_balance = fromWei(res)
                                  // console.log( data )

                                  let url_ethplorer = 'https://api.ethplorer.io/getAddressInfo/'+web3_user_address+'?apiKey=freekey'
                                  console.log( url_ethplorer )

                                  axios.get(url_ethplorer).then((res) => {
                                      // let json = CircularJSON.stringify(res);
                                      // json = CircularJSON.parse(json)
                                      return res.data;
                                  }).then((ethplorer) => {

                                      let responseData = {
                                          status: web3_data.status,
                                          web3: web3_data.web3,
                                          user_address: web3_user_address,
                                          user_balance: web3_user_balance,
                                          contract_balance: web3_contract_balance,
                                          ethplorer: ethplorer
                                      }
                                      // console.log(responseData)
                                      resolve(responseData)

                                  }).catch(err => { throw err });


                              } else {
                                  console.error(error)
                              }
                          })


                      } else {
                        console.log(err);
                      }
                    })
                }

            })

    })

    return response

}


// USER DEPOSIT
export async function depositWeb3(value) {

    let response = await new Promise(function(resolve, reject) {
        bursaInstance.deposit({value: toWei(value)}, function(err, res) {
            if(!err) {
                resolve(res)
                return res
            } else {
                console.log(err)
            }
        })
    })

    Divider('DEPOSIT')
    console.log(response)
    return response

}



// USER WITHDRAWAL
export async function withdrawalWeb3(value) {

    let response = await new Promise(function(resolve, reject) {
        bursaInstance.withdraw(toWei(value), function(err, res) {
            if(!err) {
                resolve(res)
                return res
            } else {
                console.log(err)
            }
        })
    })

    Divider('WITHDRAWAL')
    console.log(response)
    return response

}






// WILL SELL INFO (SELL/ASKS)
export async function web3ordersBook(token_address) {

    let data = initWeb3()
    // let defaultTokenInstance = initDefaultToken(token_address)

    let orders = [
        // {
        //     type: 'sell',
        //     user_address: 0,
        //     price: 10,
        //     amount: 10,
        //     total: 100,
        //     spot_id: 0
        // }
    ]

    function arrPrepare(spot_id, type, res) {

        let wei_price = fromWei(res[1])
        let wei_amount = Number(res[2])

        if(wei_price && wei_amount) {

            // let price = fromWei(scientificToDecimal(wei_price)).substr(0,18)
            // console.log(res[1])
            // let price = fromWei(parseFloat(res[1]).toPrecision(30).split('e')[0].substr(0,18))
            let price = wei_price
            // let amount = ((fromWei(wei_amount)/10)*10).toPrecision(8)
            // let amount = wei_amount.toString()
            //     amount = Number(amount)
            let amount = (wei_amount / toWei(1))
                amount = Number(amount).toPrecision(8)
            //     amount = amount.toPrecision(4)
            // let total = (price * fromWei(wei_amount)).toPrecision(8)
            // let total = (price * amount).toPrecision(8)
            let total = 0

            // let ifExist = false
            // orders.map((item, index) => {
            //     if(item.amount == amount && item.price == price) {
            //         ifExist = true
            //     }
            // })
            // if(!ifExist && amount < 99999) {
            // if(!ifExist) {
            if(price != 0) {
                let item = {
                    type: type,
                    user_address: res[0],
                    price: price,
                    amount: amount,
                    total: total,
                    spot_id: spot_id,

                    wei_amount: wei_amount,
                    wei_price: wei_price,
                }
                console.log(item)
                orders.push(item)
            }
            // }

        }
    }

    async function fetchOrderBySpot(spot_id, type) {
        // console.log(spot_id)
        try {
            if(type == 'sell') {
                await bursaInstance.willsellInfo(token_address, spot_id, function(err, res) {
                    if(!err) arrPrepare(spot_id, type, res)
                    else console.log(err)
                })
            } else if(type == 'buy') {
                await bursaInstance.willbuyInfo(token_address, spot_id, function(err, res) {
                    if(!err) arrPrepare(spot_id, type, res)
                    else console.log(err)
                })
            }
        } catch (e) {
            console.log(e)
          // return 'caught';
        }
    }

    for (let i = 0; i < 30; i++) {
        await new Promise(resolve => setTimeout(resolve, Math.random() * 100));
        // console.log(i);
        fetchOrderBySpot(i, 'sell');
        fetchOrderBySpot(i, 'buy');
    }

    // console.log('exit()')
    // console.log(orders.length)

    if(orders.length > 0) {
        orders = _.orderBy(orders, ['wei_price'], ['desc']);
        return orders
    } else {
        return false
    }

}




var defaultTokenAbi = [{"constant":false,"inputs":[{"name":"spender","type":"address"},{"name":"value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"from","type":"address"},{"name":"to","type":"address"},{"name":"value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"who","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"to","type":"address"},{"name":"value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"owner","type":"address"},{"name":"spender","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint2html56"}],"name":"Transfer","type":"event"}]


// let testTokenInstance = false
function initDefaultToken(token) {
    if(typeof web3 !== 'undefined') {
        return web3.eth.contract(defaultTokenAbi).at(token)
    }
}


// Разрешаем бирже списывать наши токены с аккаунта (если не указано второе значение, то разрешаем все токены)
export async function web3approve(init) {

    let defaultTokenInstance = initDefaultToken(init.token)

    // нужно апрувить ровно столько сколько есть на балансе, не больше
    let response = await new Promise(function(resolve, reject) {
        defaultTokenInstance.approve(BursaContract.address, toWei(init.value), function(err, res){
          if(!err) {
              resolve(res)
          } else {
              console.log(err)
          }
        })
    })

    return response
}


export async function web3orderSell(init) {

    // console.log(init)

    let response = new Promise(function(resolve, reject) {
        // bursaInstance.willsell(6*100000000000000000, tokenAddress, 1000, 1, function(err, res){
        bursaInstance.willsell(toWei(init.amount), init.token, toWei(init.price), init.spot_id, function(err, res) {
            // alert('order sell...')
            if(!err) {
                // console.log('————————')
                console.log(res)
                resolve(res)
            } else {
                console.log(err)
            }
        })
    })

    return response
}


export async function web3orderBuy(init) {

    let defaultTokenInstance = initDefaultToken(init.token)

    console.log(init)

    let response = new Promise(function(resolve, reject) {
        bursaInstance.willbuy(toWei(init.amount), init.token, toWei(init.price), init.spot_id, function(err, res) {
            // alert('order buy...')
            if(!err) {
                // console.log('————————')
                console.log(res)
                resolve(res)
            } else {
                console.log(err)
            }
        })
    })

    return response
}


export async function web3findEmptySpot(init) {

    let response = new Promise(function(resolve, reject) {
        if(init.type == 'sell') {
            bursaInstance.willsellFindSpot(init.token, function(err, res){
                console.log('SPOT SELL')
                console.log(res)
                resolve(parseFloat(res))
            })
        } else if(init.type == 'buy') {
            bursaInstance.willbuyFindSpot(init.token, function(err, res){
                console.log('SPOT BUY')
                console.log(res)
                resolve(parseFloat(res))
            })
        }
    })

    return response
}


// Узнаем сколько токенов на балансе у аккаунта
export async function web3balanceOf(token_address, user_address) {
    let defaultTokenInstance = initDefaultToken(token_address)

    let response = await new Promise(function(resolve, reject) {
        defaultTokenInstance.balanceOf(user_address, function(err, res){
          if(!err) {
              let balance = ( ((parseFloat(res)).toPrecision(50))/toWei(1) )
              resolve(balance)
              return balance
          } else {
              console.log(err)
          }
        })
    })

    return response
}

export async function web3balanceForToken(token_address, user_address) {

    let defaultTokenInstance = initDefaultToken(token_address)

    let response = await new Promise(function(resolve, reject) {
        bursaInstance.balanceApprovedForToken(token_address, user_address, function(err, res){


          if(!err) {
              let result = res / toWei(1)
              // result = result.toPrecision(8)
              // console.log( fromWei(parseFloat(res)) )
              // console.log(Web3.utils.fromWei(parseFloat(res), "ether"))
              resolve(result)
              return result
          } else {
              console.log(err)
          }
        })
    })

    return response
}



export async function web3orderCancel(init) {
    let response = await new Promise(function(resolve, reject) {


        if(init.type == 'sell') {
            console.log('SELL')
            console.log(init)
            bursaInstance.willbuy(toWei(0), init.token, toWei(init.price), init.spot_id, function(err, res){
                  resolve(res)
                  return res
            })
        } else if(init.type == 'buy') {
            console.log('BUY')
            console.log(init)
            bursaInstance.willsell(toWei(0), init.token, toWei(init.price), init.spot_id, function(err, res){
                  resolve(res)
                  return res
            })
        }
    })
    return response
}



// async function collectOrders(block) {
// 	var blockNumber = await web3.eth.getBlockNumber()
// 	var orders = await bursaInstance.getPastEvents('Order', {
// 		fromBlock: block,
// 		toBlock: blockNumber
// 	}, async function(e, order) {});
//
// 	// tmp TODO REMOVE
// 	data.orders = [];
//
// 	var i = 0;
// 	while (i < orders.length) {
// 		var o = orders[i].returnValues;
// 		var amountLeft = await bursaInstance.methods.amountLeft(o.tokenGet, o.amountGet, o.tokenGive, o.amountGive, o.block, o.user).call({from: acc0});
// 		if (amountLeft > 0) {
// 			var ord = await getOrderData(o);
// 			data.orders.push(ord);
// 		}
// 		++i;
// 	}
// 	data.block = blockNumber;
//
// 	data.orders = data.orders.sort(function(a, b) {
// 		if (a.priceOfTokenGet == b.priceOfTokenGet) {
// 			return a.event.block - b.event.block;
// 		}
// 		return b.priceOfTokenGet - a.priceOfTokenGet;
// 	});
//
// 	await saveData();
// }



/*
instantiateContract() {

  const contract = require('truffle-contract')
  const simpleStorage = contract(SimpleStorageContract)
  simpleStorage.setProvider(this.state.web3.currentProvider)

  // Declaring this for later so we can chain functions on SimpleStorage.
  var simpleStorageInstance

  // Get accounts.
  this.state.web3.eth.getAccounts((error, accounts) => {
    simpleStorage.deployed().then((instance) => {
      simpleStorageInstance = instance

      // Stores a given value, 5 by default.
      return simpleStorageInstance.set(5, {from: accounts[0]})
    }).then((result) => {
      // Get the value from the contract to prove it worked.
      return simpleStorageInstance.get.call(accounts[0])
    }).then((result) => {
      // Update state with the result.
      return this.setState({ storageValue: result.c[0] })
    })
  })
}
*/



// export default getWeb3




// HELP FUNCTIONS

    function Divider(value='Web3') {
        let d = '-----------'
        console.log(d+value+d)
    }
    // Divider()



    function toWei(price) {
      return Web3.utils.toWei(price.toString(), 'ether');
    }
    function fromWei(price) {
      return Web3.utils.fromWei(price.toString(), 'ether');
    }





    function scientificToDecimal(num) {
        //if the number is in scientific notation remove it
        if(/\d+\.?\d*e[\+\-]*\d+/i.test(num)) {
            var zero = '0',
                parts = String(num).toLowerCase().split('e'), //split into coeff and exponent
                e = parts.pop(),//store the exponential part
                l = Math.abs(e), //get the number of zeros
                sign = e/l,
                coeff_array = parts[0].split('.');
            if(sign === -1) {
                num = zero + '.' + new Array(l).join(zero) + coeff_array.join('');
            }
            else {
                var dec = coeff_array[1];
                if(dec) l = l - dec.length;
                num = coeff_array.join('') + new Array(l+1).join(zero);
            }
        }

        // num = num.substr(0,18)

        return num;
    };
