import * as constants from '../constants'
const _ = require('lodash');

const initialState = {
  menu_left: false,
  menu_right: true,
  menu_right_account: true,
  coins_data: [],
  coins_arr: [],
  selected_coins: ['EOS','TRX','VEN'],
  windowSize: [0,0],
  web3Metamask: false,
  web3Transactions: [],
}

function windowResize() {
    window.scrollTo(0,0);
    setTimeout(function() {
        window.dispatchEvent(new Event('resize'));
    }, 300);
}

export default function configUpdate(state = initialState, { type, payload }) {

  // console.log('-------------//')
  // console.log(type)
  // console.log(payload)

  switch (type) {

    case 'WINDOW_RESIZE':
        state.windowSize = payload
        return {...state}




    case 'SAVE_WEB3_METAMASK':
        state.web3Metamask = payload
        return {...state}

    case 'SAVE_WEB3_TRANSACTIONS':
        let data = state.web3Transactions
        let user_address = payload.user_address
        let transactions = payload.transactions
        if(!data[user_address]) {
            data[user_address] = []
        }
        data[user_address].push(transactions)
        state.web3Transactions = data

        return {...state}


    case 'HIDE_ALL_MENUS':
        state.menu_left = false
        state.menu_right = false
        windowResize()
        return {...state}
    case 'LEFT_MENU_TOGGLE':
        if(payload == 'toggle') {
            state.menu_left = !state.menu_left
        } else {
            state.menu_left = payload
        }
        windowResize()
        return {...state}
    case 'RIGHT_MENU_TOGGLE':

        state.menu_right_account = true

        if(payload == 'toggle') {
            state.menu_right = !state.menu_right
        } else if(payload == 'hideAccount') {
            state.menu_right = true
            state.menu_right_account = false
        } else {
            state.menu_right = payload
        }

        windowResize()
        return {...state}


    case 'PRELOAD_COINS':
        state.coins_arr = _.values(payload.coins)
        state.coins_data = payload.data
        return {...state}

    case 'SELECT_COINS':

        let coins_arr = state.coins_arr
        let selected_coins = state.selected_coins

        if(payload.type) {
            payload.array.map((item, index) => {
                let coin = item.toUpperCase()
                if(coins_arr.includes(coin)) {
                    selected_coins.push(coin)
                    state.selected_coins = Array.from(new Set(selected_coins))
                }
            })
        } else {
            payload.array.map((item, index) => {
                state.selected_coins = selected_coins.filter(e => e !== item)
            })
        }

        return {...state}

    default:
      return state
  }
}
