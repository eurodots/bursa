var compiled = require('./BursaCompiled.js');


var bursa;
var tok1;

var con;

var assert = require('assert');
const fs = require('fs');
const Web3 = require('web3');


try {
  var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
} catch (e) {
  console.log(e);
}

function toWei(amount) {
  return web3.utils.toWei(amount.toString(), 'ether');
}
function fromWei(amount) {
  return web3.utils.fromWei(amount.toString(), 'ether');
}



var acc;
async function deploy() {
  acc = await web3.eth.getAccounts();
  web3.eth.coinbase = acc[0];
  web3.eth.defaultAccount = acc[0];
  // console.dir(acc);

  const Tok1 = new web3.eth.Contract(compiled.tok1_abi);
  tok1 = await Tok1.deploy({
      data: compiled.tok1_data,
  }).send({
      from: acc[0],
      gas: 5000000,
      gasPrice: 20000000000
  });
  console.log(`Token contract deployed at ${tok1.options.address}`);
  tok1.setProvider(web3.currentProvider)


  const Bursa = new web3.eth.Contract(compiled.bursa_abi);
  bursa = await Bursa.deploy({
      data: compiled.bursa_data,
//      arguments: [acc[0],acc[0],0,5,5],
  }).send({
      from: acc[0],
      gas: 5000000,
      gasPrice: 20000000000
  });
  console.log(`Contract deployed at ${bursa.options.address}`);
  bursa.setProvider(web3.currentProvider)

}


var ethUtils = require('ethereumjs-util');


async function test() {
  await deploy();


  try {
  var b1,b2,b3,b4,tb1,tb2,tb3,tb4,bb1,bb2,bb3,bb4,dd1,dd2,dd3,dd4, tqb1,tqb2,tqb3,tqb4,bbb,bbbb;
  async function show() {
    // var b = await bursa.methods.balanceOf(acc[1]).call({ from: acc[1] });
    var prev_bb1 = bb1;
    var prev_bb2 = bb2;
    var prev_bb3 = bb3;
    var prev_bb4 = bb4;
    var prev_dd1 = dd1;
    var prev_dd2 = dd2;
    var prev_dd3 = dd3;
    var prev_dd4 = dd4;
    var prev_bbb = bbb;
    console.log('acc1 = [' + acc[1].slice(38) + ']');
    console.log('acc2 = [' + acc[2].slice(38) + ']');
    console.log('acc3 = [' + acc[3].slice(38) + ']');
    console.log('acc4 = [' + acc[4].slice(38) + ']');
    console.log(bb1 = fromWei(await web3.eth.getBalance(acc[1]), bb1 - prev_bb1));
    console.log(bb2 = fromWei(await web3.eth.getBalance(acc[2]), bb2 - prev_bb2));
    console.log(bb3 = fromWei(await web3.eth.getBalance(acc[3]), bb3 - prev_bb3));
    console.log(bb4 = fromWei(await web3.eth.getBalance(acc[4]), bb4 - prev_bb4));
    console.log('token1 token balance  acc1 = ', b1 = fromWei(await tok1.methods.balanceOf(acc[1]).call({ from: acc[1] })));
    console.log('token1 token balance  acc2 = ', b2 = fromWei(await tok1.methods.balanceOf(acc[2]).call({ from: acc[2] })));
    console.log('token1 token balance  acc3 = ', b3 = fromWei(await tok1.methods.balanceOf(acc[3]).call({ from: acc[3] })));
    console.log('token1 token balance  acc4 = ', b4 = fromWei(await tok1.methods.balanceOf(acc[4]).call({ from: acc[4] })));
    console.log('token1 token balance TOTAL = ', parseFloat(b1) + parseFloat(b2) + parseFloat(b3) + parseFloat(b4));
    console.log('token1 token balance bursa = ', fromWei(await tok1.methods.balanceOf(bursa.options.address).call({ from: acc[0] })));
    console.log('coupon balance  acc1 = ', tqb1 = fromWei(await bursa.methods.balanceOf(acc[1]).call({ from: acc[1] })));
    console.log('coupon balance  acc2 = ', tqb2 = fromWei(await bursa.methods.balanceOf(acc[2]).call({ from: acc[2] })));
    console.log('coupon balance  acc3 = ', tqb3 = fromWei(await bursa.methods.balanceOf(acc[3]).call({ from: acc[3] })));
    console.log('coupon balance  acc4 = ', tqb4 = fromWei(await bursa.methods.balanceOf(acc[4]).call({ from: acc[4] })));
    console.log('coupon balance TOTAL = ', parseFloat(tqb1) + parseFloat(tqb2) + parseFloat(tqb3) + parseFloat(tqb4));
    console.log('coupon balance  acc0 = ', fromWei(await bursa.methods.balanceOf(acc[0]).call({ from: acc[0] })));
    console.log('\n');

  }

  async function balance(account, token) {
    return await token.methods.balanceOf(account).call({ from: account });
  }

  async function getTokenName(token) {
    try {
      return await token.methods.name().call({ from: acc[0] });
    } catch (e) {
      return '??';
    }
  }
  async function mint(amount, token, account) {
    var tx = await token.methods.mint(amount, account).send({ from: acc[0], gas:4000000});
    var name = await getTokenName(token);
    console.log('Mint ', amount + ' ' + name + ' for ['+ account.slice(38) +']  (', tx.gasUsed, 'gas )');
  }
  async function approve(amount, token, from, to) {
    var tx = await token.methods.approve(to, amount).send({ from: from, gas:4000000});
    var name = await getTokenName(token);
    console.log('Approve Bursa ', amount + ' ' + name + ' from ['+ from.slice(38) +']  (', tx.gasUsed, 'gas )');
  }
  async function sendEther(value, from, to) {
    var tx = await web3.eth.sendTransaction({from:from, to:to, value: value});
    console.log('Send ether from ', value + ' from [' + from.slice(38) + ']  (', tx.gasUsed, 'gas )');
  }
  async function transferToken(amount, token, from, to) {
    console.log('transferToken()');
    var start_b1 = await balance(from,token);
    var start_b2 = await balance(to,token);
    var tx = await token.methods.transfer(to, amount).send({ from: from, gas:4000000});
    var name = await getTokenName(token);
    var end_b1 = await balance(from,token);
    var end_b2 = await balance(to,token);
    // console.log('amount', amount);
    // console.log('start_b1',start_b1);
    // console.log('start_b2', start_b2);
    // console.log('end_b1  ', end_b1);
    // console.log('end_b2  ', end_b2);
    // console.log(start_b1 - end_b1);
    // console.log(end_b2 - start_b2);
    assert.equal(amount, start_b1 - end_b1);
    assert.equal(amount, end_b2 - start_b2);
    console.log('Transfer ', amount + ' ' + name + ' from ['+ from.slice(38) +'] to [' + to.slice(38) + '] (' + tx.gasUsed + ' gas )');
  }
  async function transferFromToken(amount, token, from, to, sender) {
    var start_b1 = await balance(from,token);
    var start_b2 = await balance(to,token);
    var tx = await token.methods.transferFrom(from, to, amount).send({ from: sender, gas:4000000});
    var name = await getTokenName(token);
    var end_b1 = await balance(from,token);
    var end_b2 = await balance(to,token);
    // console.log(start_b1);
    // console.log(start_b2);
    // console.log(end_b1);
    // console.log(end_b2);
    assert.equal(amount, start_b1 - end_b1);
    assert.equal(amount, end_b2 - start_b2);
    console.log('TransferFrom ', amount + ' ' + name + ' from ['+ from.slice(38) +'] to [' + to.slice(38) + '] (' + tx.gasUsed + ' gas )');
  }


  async function amountLeft(text, order) {
    var tokenGet = 0;
    var tokenGive = 0;
    if (order.tokenGet != 0) {
      tokenGet = order.tokenGet.options.address;
    }
    if (order.tokenGive != 0) {
      tokenGive = order.tokenGive.options.address;
    }
    var out = await bursa.methods.amountLeft(tokenGet, order.amountGet, tokenGive, order.amountGive, order.block, order.user).call({ from: order.user });
    console.log('amountLeft(' + text + ') = ' + out)
  }

  async function willBuy(amountGet, tokenGet, amountGive, tokenGive, from) {
    var name1 = '';
    var name2 = '';
    var t1 = tokenGet;
    var t2 = tokenGive;
    if (tokenGet != 0) {
      name1 = await getTokenName(tokenGet);
      t1 = tokenGet.options.address;
    }
    if (tokenGive != 0) {
      name2 = await getTokenName(tokenGive);
      t2 = tokenGive.options.address;
    }

    var tx = await bursa.methods.order(t1, amountGet, t2, amountGive).send({ from: from });

    var order = {
      amountGet:amountGet,
      tokenGet:tokenGet,
      amountGive:amountGive,
      tokenGive:tokenGive,
      user:from,
      block:tx.blockNumber
    };
    console.log('[' + from.slice(38), '] says:\n -> WILL BUY', amountGet, name1, 'for', amountGive, name2, ' (', tx.gasUsed, 'gas )');
    await amountLeft("", order);
    console.log();
    order.events = tx.events;
    return order;
  }

  async function makeTrade(order, factAmountGet, from) {
    var start_b1_tokenGet = await balance(order.user, order.tokenGet);
    var start_b1_tokenGive = await balance(order.user, order.tokenGive);
    var start_b2_tokenGet = await balance(from, order.tokenGet);
    var start_b2_tokenGive = await balance(from, order.tokenGive);

    if (factAmountGet) {
      factAmountGet = await canTrade(order, factAmountGet, from);
      if (factAmountGet == 0) return;
    } else {
      var r = await canTradeCollectible(order, from);
      if (r == false) return;
    }
    var nameGet = '';
    var nameGive = '';
    var tokenGet = 0;
    var tokenGive = 0;
    if (order.tokenGet != 0) {
      nameGet = await getTokenName(order.tokenGet);
      tokenGet = order.tokenGet.options.address;
    }
    if (order.tokenGive != 0) {
      nameGive = await getTokenName(order.tokenGive);
      tokenGive = order.tokenGive.options.address;
    }
    var tx = await bursa.methods.trade(tokenGet, order.amountGet, tokenGive, order.amountGive,
      order.block, order.user, factAmountGet, from).send({ from: from, gas: 5000000 });
    if (factAmountGet == 0) {
      factAmountGet == order.amountGet;
    }
    if (factAmountGet) {
      console.log('[' + from.slice(38), '] says:\n -> MAKE TRADE', factAmountGet, '/', order.amountGet, nameGet, 'for', factAmountGet/order.amountGet*order.amountGive, '/', order.amountGive, nameGive, ' (', tx.gasUsed, 'gas )');
      await amountLeft("", order);
    } else {
      console.log('[' + from.slice(38), '] says:\n -> MAKE TRADE COLLECTIBLE', order.amountGet, nameGet, 'for', order.amountGive, nameGive, '\nwith [', order.user.slice(38), '] (', tx.gasUsed, 'gas )');
    }
    console.log();
    await show();


    var end_b1_tokenGet = await balance(order.user, order.tokenGet);
    var end_b1_tokenGive = await balance(order.user, order.tokenGive);
    var end_b2_tokenGet = await balance(from, order.tokenGet);
    var end_b2_tokenGive = await balance(from, order.tokenGive);

    console.log('tokenGet b1 ' + start_b1_tokenGet + ' -> \n            ' + end_b1_tokenGet);
    console.log('tokenGet b2 ' + start_b2_tokenGet + ' -> \n            ' + end_b2_tokenGet);
    console.log('tokenGive b1 ' + start_b1_tokenGive + ' -> \n             ' + end_b1_tokenGive);
    console.log('tokenGive b2 ' + start_b2_tokenGive + ' -> \n             ' + end_b2_tokenGive);
    assert.equal(start_b2_tokenGet - end_b2_tokenGet, end_b1_tokenGet - start_b1_tokenGet);
  }


  async function cancelOrder(order, from) {
    var nameGet = '';
    var nameGive = '';
    var tokenGet = 0;
    var tokenGive = 0;
    if (order.tokenGet != 0) {
      nameGet = await getTokenName(order.tokenGet);
      tokenGet = order.tokenGet.options.address;
    }
    if (order.tokenGive != 0) {
      nameGive = await getTokenName(order.tokenGive);
      tokenGive = order.tokenGive.options.address;
    }
    var tx = await bursa.methods.cancelOrder(tokenGet, order.amountGet, tokenGive, order.amountGive,
      order.block).send({ from: from, gas: 5000000 });
    console.log('[' + from.slice(38), '] says:\n -> CANCEL ORDER', order.amountGet, nameGet, 'for', order.amountGive, nameGive, ' (', tx.gasUsed, 'gas )');
    await amountLeft("", order);
    console.log();
  }


  async function canTrade(order, factAmountGet, from) {
    var nameGet = '';
    var nameGive = '';
    var tokenGet = 0;
    var tokenGive = 0;
    if (order.tokenGet != 0) {
      nameGet = await getTokenName(order.tokenGet);
      tokenGet = order.tokenGet.options.address;
    }
    if (order.tokenGive != 0) {
      nameGive = await getTokenName(order.tokenGive);
      tokenGive = order.tokenGive.options.address;
    }
    var b = await bursa.methods.canTrade(tokenGet, order.amountGet, tokenGive, order.amountGive,
      order.block, order.user, factAmountGet).call({ from: from });
    if (factAmountGet == 0) {
      factAmountGet == order.amountGet;
    }
    console.log('[' + from.slice(38), '] says:\n -> CAN TRADE', b, '. For trade', factAmountGet, '/', order.amountGet, nameGet, 'for', factAmountGet/order.amountGet*order.amountGive, '/', order.amountGive, nameGive);
    console.log();
    return b;
  }



  async function _canBuyCollectible(order, from) {
    var nameGet = '';
    var nameGive = '';
    var tokenGet = 0;
    var tokenGive = 0;
    if (order.tokenGet != 0) {
      nameGet = await getTokenName(order.tokenGet);
      tokenGet = order.tokenGet.options.address;
    }
    if (order.tokenGive != 0) {
      nameGive = await getTokenName(order.tokenGive);
      tokenGive = order.tokenGive.options.address;
    }
    var b;
    try {
      b = await bursa.methods.canBuyCollectible(tokenGet, order.amountGet, tokenGive, order.amountGive,
        order.block, order.user).call({ from: from });
      console.log('[' + from.slice(38), '] says:\n -> CAN BUY COLLECTIBLE', order.amountGet, nameGet, 'for', order.amountGive, nameGive, '\nwith [', order.user.slice(38), ']');
      console.log();
      return b;
    } catch (e) {
      console.log('[' + from.slice(38), '] says:\n -> CANNOT BUY COLLECTIBLE', order.amountGet, nameGet, 'for', order.amountGive, nameGive, '\nwith [', order.user.slice(38), ']');
      console.log();
      return false;
    }
  }

  async function _canSellCollectible(order, from) {
    var nameGet = '';
    var nameGive = '';
    var tokenGet = 0;
    var tokenGive = 0;
    if (order.tokenGet != 0) {
      nameGet = await getTokenName(order.tokenGet);
      tokenGet = order.tokenGet.options.address;
    }
    if (order.tokenGive != 0) {
      nameGive = await getTokenName(order.tokenGive);
      tokenGive = order.tokenGive.options.address;
    }
    var b;
    try {
      b = await bursa.methods.canSellCollectible(tokenGet, order.amountGet, tokenGive, order.amountGive,
        order.block, order.user).call({ from: from });
      console.log('[' + from.slice(38), '] says:\n -> CAN SELL COLLECTIBLE', order.amountGet, nameGet, 'for', order.amountGive, nameGive, '\nwith [', order.user.slice(38), ']');
      console.log();
      return b;
    } catch (e) {
      console.log('[' + from.slice(38), '] says:\n -> CANNOT SELL COLLECTIBLE', order.amountGet, nameGet, 'for', order.amountGive, nameGive, '\nwith [', order.user.slice(38), ']');
      console.log();
      return false;
    }
  }

  async function canTradeCollectible(order, from) {
    var a = await _canBuyCollectible(order, from);
    var b = await _canSellCollectible(order, from);
    if (a || b) {
      return true;
    } else return false;
  }











/*   TESTS    */



  await show();


  async function bestBuy(token, max_price) {
    var out = await bursa.methods.findBestBuy(token.options.address, toWei(max_price)).call({ from: acc[1] });
    console.log('bestBuy id is:   order ', out[0] + ', volume', fromWei(out[1]) + ', price', fromWei(out[2]))
    return out.order;
  }
  async function bestSell(token, min_price) {
    var out = await bursa.methods.findBestSell(token.options.address, toWei(min_price)).call({ from: acc[1] });
    console.log('bestSell id is:  order ', out[0] + ', volume', fromWei(out[1]) + ', price', fromWei(out[2]))
    return out.order;
  }
  async function amountOnSellOrder(token, order) {
    var out = await bursa.methods.check_volume_sell(token.options.address, order).call({ from: acc[1] });
    console.log('check_volume_sell on order ' + order + ': ' + fromWei(out))
  }
  async function amountOnBuyOrder(token, order) {
    var out = await bursa.methods.check_volume_buy(token.options.address, order).call({ from: acc[1] });
    console.log('check_volume_buy on order  ' + order + ': ' + fromWei(out))
  }
  async function priceSellOrder(token, order) {
    var out = await bursa.methods.checkPriceSell(token.options.address, order).call({ from: acc[1] });
    console.log('checkPriceSell on order  ' + order + ': ' + fromWei(out))
  }
  async function priceBuyOrder(token, order) {
    var out = await bursa.methods.checkPriceBuy(token.options.address, order).call({ from: acc[1] });
    console.log('checkPriceBuy on order   ' + order + ': ' + fromWei(out))
  }

  async function buy(amount, token, max_price, order, refund, from, money) {
    var tx = await bursa.methods.buy(toWei(amount), token.options.address, toWei(max_price), order, refund).send({ from: from, gas:4000000, value: toWei(money)});
    var name = await getTokenName(token);
    console.log('buy ', amount + ' ' + name + ' from ['+ from.slice(38) +']  (', tx.gasUsed, 'gas )');
    await show();
  }
  async function sell(amount, token, min_price, order, refund, from, money) {
    var tx = await bursa.methods.sell(toWei(amount), token.options.address, toWei(min_price), order, refund).send({ from: from, gas:4000000, value: toWei(money)});
    var name = await getTokenName(token);
    console.log('sell ', amount + ' ' + name + ' from ['+ from.slice(38) +']  (', tx.gasUsed, 'gas )');
    await show();
  }



  async function testInit(contract) {

    await mint(toWei(100), tok1, acc[1]);
    await mint(toWei(100), tok1, acc[2]);
    await mint(toWei(100), tok1, acc[3]);
    await mint(toWei(100), tok1, acc[4]);

    await approve(toWei(100), tok1, acc[1], bursa.options.address);
    await approve(toWei(100), tok1, acc[2], bursa.options.address);
    await approve(toWei(100), tok1, acc[3], bursa.options.address);
    await approve(toWei(100), tok1, acc[4], bursa.options.address);

    await sendEther(toWei(2), acc[1], bursa.options.address);
    await sendEther(toWei(2), acc[2], bursa.options.address);
    await sendEther(toWei(2), acc[3], bursa.options.address);
    await sendEther(toWei(2), acc[4], bursa.options.address);
    console.log('\n');
    await show();
  }


  async function testSell() {
    async function willBuy(amount, token, price, spot, from) {
      var tx = await bursa.methods.willbuy(toWei(amount), token.options.address, toWei(price), spot).send({ from: from, gas:4000000});
      var name = await getTokenName(token);
      console.log('willBuy ', amount + ' ' + name + ' at ' + price + ' to ['+ from.slice(38) +']  (', tx.gasUsed, 'gas )');
    }

    await willBuy(20, tok1, 0.10, 1, acc[1]);
    await willBuy(56, tok1, 0.08, 2, acc[2]);
    await willBuy(30, tok1, 0.07, 3, acc[1]);
    // await willBuy(88, tok1, 0.06, 4, acc[2]);
    // await willBuy(20, tok1, 0.05, 5, acc[1]);

    var r = await bestSell(tok1, 0);
    await sell(4, tok1, 0, r, 0, acc[4], 0);
    var r = await bestSell(tok1, 0);
    await sell(10, tok1, 0, r, 0, acc[4], 0);
    var r = await bestSell(tok1, 0);
    await sell(20, tok1, 0, r, 0, acc[4], 0);

  }

  async function testBuy() {
    async function willSell(amount, token, price, spot, from) {
      var tx = await bursa.methods.willsell(toWei(amount), token.options.address, toWei(price), spot).send({ from: from, gas:4000000});
      var name = await getTokenName(token);
      console.log('willSell ', amount + ' ' + name + ' at ' + price + ' to ['+ from.slice(38) +']  (', tx.gasUsed, 'gas )');
    }

    await willSell(110,tok1, 0.007, 1, acc[1]);
    await willSell(50, tok1, 0.008, 2, acc[2]);
    await willSell(30, tok1, 0.009, 3, acc[3]);
    await willSell(60, tok1, 0.010, 4, acc[4]);

    var r = await bestBuy(tok1, 0);
    await buy(10, tok1, 0, r, 0, acc[4], 0.1);
    var r = await bestBuy(tok1, 0);
    await buy(5, tok1, 0, r, 0, acc[4], 0);
    var r = await bestBuy(tok1, 0);
    await buy(10, tok1, 0, r, 0, acc[4], 0);
    var r = await bestBuy(tok1, 0);
    await buy(5, tok1, 0, r, 0, acc[4], 0);
    var r = await bestBuy(tok1, 0);

  }






  async function testWithdraw() {
    // withdrqw all ether acc1
    var b1 = await bursa.methods.balanceOf(acc[1]).call({ from: acc[1] });
    var tx = await bursa.methods.withdraw(b1).send({ from: acc[1], gas: 5000000 });
    console.log('-> withdraw all ether acc1 (', tx.gasUsed, 'gas )');

    await show();
  }




// TODO rewrite tests

await testInit();
await testSell();
// await testBuy();
// await testWithdraw();


} catch(e) {
  console.log('FAIL', e);
  return;
}






}


test();
