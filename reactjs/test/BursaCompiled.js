const fs = require('fs');
var solc = require('solc')

async function compile() {
  var bursa_abi;
  var bursa_data;
  var tok1_abi;
  var tok1_data;
  var tron_abi;
  var tron_data;
  var erc721_abi;
  var erc721_data;


  await fs.readFile("bursa.sol", "utf8", async function(err, input) {
    // console.log(data);



    // var input = 'contract x { function g() {} }'
    console.log("Compile...");
    var output = solc.compile(input, 1)

    if (Object.keys(output.contracts).length === 0) {
      console.log("ERROR. Run solc");
      console.log(output.errors);
      return;
    }

    for (var contractName in output.contracts) {
     switch (contractName) {
       case ':Token1':
       console.log(contractName);
         tok1_abi = output.contracts[contractName].interface;
         tok1_data = output.contracts[contractName].bytecode;
         break;

       case ':Bursa':
       console.log(contractName);
         bursa_abi = output.contracts[contractName].interface;
         bursa_data = output.contracts[contractName].bytecode;
         break;

       default:
         console.log('Contract unused:', contractName);
      }
    }



    var text = '';
    async function addCompiled(name, abi, data) {
      text = text + 'exports.' + name + '_abi = ' + abi + ';\n\n'
                      + 'exports.' + name + '_data = \'0x' + data + '\';\n\n'
    }

    async function saveCompiled(text) {
      await fs.writeFile('compiled.js', text, 'utf8', function(err) {
        if (err) console.log('ERROR: Can\'t write to file');
         console.log('Saved.');
      });
    }



    await addCompiled("tok1", tok1_abi, tok1_data);
    await addCompiled("bursa", bursa_abi, bursa_data);
    await saveCompiled(text);
  });







}
compile();
