import pytz
import time
import datetime
import dateparser
import os
import requests

from _utils.config import sysConf


def split(arr, size):
    arrs = []
    while len(arr) > size:
        pice = arr[:size]
        arrs.append(pice)
        arr = arr[size:]
    arrs.append(arr)
    return arrs


def timePeriod(value):
    time_period = datetime.datetime.now() - datetime.timedelta(hours=value)
    time_period = int(time_period.timestamp())
    return time_period


def timestamp(format=False):
    local_tz = pytz.timezone(sysConf['timeZone'])
    datetime_without_tz = datetime.datetime.now()
    datetime_with_tz = local_tz.localize(datetime_without_tz, is_dst=None) # No daylight saving time
    datetime_in_utc = datetime_with_tz.astimezone(pytz.utc)
    if format:
        timeNow = datetime_in_utc.strftime(format)
        timeNow = dateparser.parse(timeNow)
        timeNow = time.mktime(timeNow.timetuple())
        # '%Y-%m-%d %H:%M:%S %Z'
    else:
        timeNow = time.mktime(datetime_in_utc.timetuple())

    return int(timeNow)




def d(value=False):
    print('\n')
    print('------',value,'------')
    print('\n')



