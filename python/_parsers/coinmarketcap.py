import threading
import requests
import datetime
import time
from bs4 import BeautifulSoup
from lxml import html
import json
import re
import pymysql

from _utils.functions import *
from _utils.config import *




def parsList(threads=20):


    page_url = 'https://coinmarketcap.com/tokens/views/all/'
    html = requests.get(page_url, headers=sysConf['header'])
    html = html.content.decode('utf8')
    soup = BeautifulSoup(html, "lxml")

    table_arr = soup.find('table', {'id': 'assets-all'}).find('tbody').find_all('tr', text=False)


    table_arr = split(table_arr, threads)
    # for arr in table_arr:
    #     parsListCurrent(arr)
    #     exit()

    ev = {}
    th = {}

    for index, arr in enumerate(table_arr):
        ev["x" + str(index)] = threading.Event()
        th["x" + str(index)] = threading.Thread(target=parsListCurrent, args=(arr,))
        th["x" + str(index)].start()

    ev["x1"].set()
    for i, t in enumerate(th):
        th["x" + str(i)].join()


def parsListCurrent(table_arr):

    conn = pymysql.connect(host=dbConf['host'], port=dbConf['port'], user=dbConf['user'], passwd=dbConf['passwd'], db=dbConf['name'], charset=dbConf['charset'])
    cur = conn.cursor()

    for tr in table_arr:

        try:
            td_first = tr.find('td', {'class': 'currency-name'})
            td_name = td_first.find('a', text=True).text
            td_href = td_first.find('a').get('href')

            td_platform = tr.find('td', {'class': 'platform-name'})
            td_platform = td_platform.find('a', text=True).text if td_platform else False

            td_capital = tr.find('td', {'class': 'market-cap'})
            td_capital_usd = td_capital.get('data-usd')
            td_capital_btc = td_capital.get('data-btc')

            td_price = tr.find('a', {'class': 'price'})
            td_price_usd = td_price.get('data-usd')
            td_price_btc = td_price.get('data-btc')

            td_suply = tr.find('td', {'class': 'circulating-supply'}).find('a', text=True)
            td_suply_number = td_suply.get('data-supply') if td_suply else False

            td_volume24 = tr.find('a', {'class': 'volume'})
            td_volume24_usd = td_volume24.get('data-usd')
            td_volume24_btc = td_volume24.get('data-btc')

            td_changes = tr.find_all('td', {'class': 'percent-change'})
            td_changes_1h = False
            td_changes_24h = False
            td_changes_7d = False
            for index, td in enumerate(td_changes):

                num = td.get('data-percentusd')
                if index == 0:
                    td_changes_1h = num
                elif index == 1:
                    td_changes_24h = num
                elif index == 2:
                    td_changes_7d = num

            coinmarketcap_link = 'https://coinmarketcap.com' + str(td_href)
            extra = page(coinmarketcap_link)

            coin_arr = {
                'changes_1h': str(td_changes_1h),
                'changes_24h': str(td_changes_24h),
                'changes_7d': str(td_changes_7d),
                'volume24_usd': str(td_volume24_usd),
                'volume24_btc': str(td_volume24_btc),
                'suply_number': str(td_suply_number),
                'price_usd': str(td_price_usd),
                'price_btc': str(td_price_btc),
                'capital_usd': str(td_capital_usd),
                'capital_btc': str(td_capital_btc),
                'platform': str(td_platform),
                'name': str(td_name),
                'extra': extra,
                'updated_at': timestamp()
            }

            name = str(td_name)
            fullname = str(extra['fullname'])

            saveFavicon(name, extra['favicon'])
            # saveJson(name, coin_arr)

            query = ([
                name,
                fullname,
            ])
            cur.execute("SELECT `id` FROM `coins` WHERE `coin` = %s AND `label` = %s", query)
            coin_id = cur.fetchone()
            coin_id = coin_id[0] if coin_id else False
            # print(coin_id)
            # exit()

            if not coin_id:

                # TABLE: COINS
                query = ([
                    name,
                    fullname,
                    coin_arr['platform'],
                    extra['rank'],
                    extra['contract_address'],
                    coinmarketcap_link,
                    0,
                    timestamp()
                ])
                # print(query)
                cur.execute(
                    "INSERT INTO `coins` (`id`,`coin`,`label`,`platform`,`rank`,`contract_address`,`coinmarketcap`,`coinmarketcap_id`,`updated_at`) VALUES (Null,%s,%s,%s,%s,%s,%s,%s,%s)",
                    query)
                coin_id = cur.lastrowid
                conn.commit()

                # TABLE: COINS_LINKS
                for link in extra['links']:
                    label = link['label'].title()
                    value = link['value']
                    # print(label, value)

                    query = ([
                        coin_id,
                        label,
                        value
                    ])
                    # print(query)
                    cur.execute("INSERT INTO `coins_links` (`id`,`coin_id`,`label`,`value`) VALUES (Null,%s,%s,%s)",
                                query)
                    conn.commit()

                d(coinmarketcap_link)
                print(coin_arr)


        except Exception as e:
            d('ERROR')
            print('>>>:', e)

            # exit()

    conn.close()


def saveJson(name, arr):
    path = 'json/'+name+'.json'
    with open(path, 'w') as outfile:
        json.dump(arr, outfile)


def page(page_url):

    html = requests.get(page_url, headers=sysConf['header'])
    html = html.content.decode('utf8')
    soup = BeautifulSoup(html, "lxml")

    d(page_url)

    get_image = soup.find('img', {'class': 'currency-logo-32x32'})
    get_image_src = get_image.get('src')
    get_image_src = re.sub('32x32', '128x128', str(get_image_src))

    get_fullname = get_image.get('alt')

    get_block = soup.find('div', {'class': 'col-sm-4 col-sm-pull-8'})
    get_block_links = get_block.find_all('a')
    links_arr = []
    for link in get_block_links:
        links_arr.append(link.get('href'))


    rank = get_block.find('span', {'class': 'label label-success'})
    if rank:
        rank = rank.text
        rank = re.findall(r'\d+', rank)
        rank = int(rank[0])
    else:
        rank = False


    fields = linksModifier(links_arr)
    return_arr = {
        'coinmarketcap': page_url,
        'favicon': get_image_src,
        'links': fields['links'],
        'data': fields['data'],
        'contract_address': fields['contract_address'],
        'rank': rank,
        'fullname': str(get_fullname),
    }

    # print(return_arr)
    return return_arr

def linksModifier(arr):

    data_arr = {
        # 'contract_address': False,
        'ethplorer': False,
        'etherscan': False,
        'website': False,
        'bitcointalk': False,
        'telegram': False,
        'github': False,
        'medium': False,
    }
    contract_address = False

    links_arr = []
    for index, link in enumerate(arr):

        label = re.sub('www.', '', str(link))
        label = re.search(':\/\/(.+?)\.', label).group(1)

        if label == 't':
            label = 'Telegram'


        links_arr.append({
            'label': label,
            'value': link
        })


        if index == 0:
            data_arr['website'] = link
        elif label == 'ethplorer':
            data_arr['ethplorer'] = link
        elif label == 'etherscan':
            data_arr['etherscan'] = link
        elif label == 'bitcointalk':
            data_arr['bitcointalk'] = link
        elif label == 't':
            data_arr['telegram'] = link
        elif label == 'github':
            data_arr['github'] = link
        elif label == 'medium':
            data_arr['medium'] = link

        if label == 'ethplorer':
            contract_address = link.split('/address/')
            contract_address = contract_address[1] if contract_address[1] else False
        if not contract_address and label == 'etherscan':
            contract_address = link.split('/token/')

            d(contract_address)

            if contract_address[1]:
                contract_address = contract_address[1]
            else:
                contract_address = link.split('/address/')
                contract_address = contract_address[1] if contract_address[1] else False


    return_arr = {
        'links': links_arr,
        'data': data_arr,
        'contract_address': contract_address
    }
    # print(return_arr)
    # exit()
    return return_arr


def saveFavicon(name, url):

    url_128px = url
    url_32px = re.sub('128x128', '32x32', str(url))

    folder = '_favicons/'
    path_128px = folder + '128x128/' + name + '.png'
    path_32px  = folder + '32x32/' + name + '.png'

    print(url_128px, path_128px)
    print(url_32px, path_32px)
    # exit()

    if not os.path.isdir(folder):
        os.makedirs(folder)

    with open(path_128px, 'wb') as f:
        resp = requests.get(url_128px, verify=False)
        f.write(resp.content)

    with open(path_32px, 'wb') as f:
        resp = requests.get(url_32px, verify=False)
        f.write(resp.content)

    return True


def updateCurrentCoin(currentCoin):

    page_url = 'https://coinmarketcap.com/tokens/views/all/'
    html = requests.get(page_url, headers=sysConf['header'])
    html = html.content.decode('utf8')
    soup = BeautifulSoup(html, "lxml")

    table = soup.find('table', {'id': 'assets-all'}).find('tbody').find_all('tr', text=False)


    # data_arr = []

    for tr in table:
        td_first = tr.find('td', {'class': 'currency-name'})
        td_name = td_first.find('a', text=False).text
        td_href = td_first.find('a').get('href')

        td_coinmarketcap_id = td_first.find('div', {'class': 'currency-logo-sprite'})
        if td_coinmarketcap_id:
            td_coinmarketcap_id = td_coinmarketcap_id.get('class')[0]
            td_coinmarketcap_id = td_coinmarketcap_id.split('-')[-1:][0]
        else:
            td_coinmarketcap_id = False


        td_platform = tr.find('td', {'class': 'platform-name'})
        td_platform = td_platform.find('a', text=True).text if td_platform else False

        td_capital = tr.find('td', {'class': 'market-cap'})
        td_capital_usd = td_capital.get('data-usd')
        td_capital_btc = td_capital.get('data-btc')

        td_price = tr.find('a', {'class': 'price'})
        td_price_usd = td_price.get('data-usd')
        td_price_btc = td_price.get('data-btc')

        td_suply = tr.find('td', {'class': 'circulating-supply'}).find('a', text=True)
        td_suply_number = td_suply.get('data-supply') if td_suply else False


        td_volume24 = tr.find('a', {'class': 'volume'})
        td_volume24_usd = td_volume24.get('data-usd')
        td_volume24_btc = td_volume24.get('data-btc')


        td_changes = tr.find_all('td', {'class': 'percent-change'})
        td_changes_1h = False
        td_changes_24h = False
        td_changes_7d = False
        for index, td in enumerate(td_changes):

            num = td.get('data-percentusd')
            if index == 0:
                td_changes_1h = num
            elif index == 1:
                td_changes_24h = num
            elif index == 2:
                td_changes_7d = num

        if str(td_name).upper() == currentCoin.upper():
            coinmarketcap_link = 'https://coinmarketcap.com'+str(td_href)
            extra = page(coinmarketcap_link)

            coin_arr = {
                'changes_1h': str(td_changes_1h),
                'changes_24h': str(td_changes_24h),
                'changes_7d': str(td_changes_7d),
                'volume24_usd': str(td_volume24_usd),
                'volume24_btc': str(td_volume24_btc),
                'suply_number': str(td_suply_number),
                'price_usd': str(td_price_usd),
                'price_btc': str(td_price_btc),
                'capital_usd': str(td_capital_usd),
                'capital_btc': str(td_capital_btc),
                'platform': str(td_platform),
                'name': str(td_name),
                'extra': extra,
                'updated_at': timestamp(),
                'coinmarketcap': coinmarketcap_link,
                'coinmarketcap_id': td_coinmarketcap_id
            }
            # data_arr.append(coin_arr)

            # print(coin_arr)
            # exit()
            return coin_arr
        # print(coin_arr)

        # exit()
    return



def updateLinks(threads=20):
    conn = pymysql.connect(host=dbConf['host'], port=dbConf['port'], user=dbConf['user'], passwd=dbConf['passwd'], db=dbConf['name'], charset=dbConf['charset'])
    cur = conn.cursor()


    try:
        cur.execute("SELECT * FROM `coins` WHERE `updated_at` < %s", (timePeriod(sysConf['parsLinksWhichOlderThenHours']),))
        coins_arr = cur.fetchall()


        coins_arr = split(coins_arr, threads)
        # for row in coins_arr:
        #     updateLinkHistory(row)
        #     exit()

        ev = {}
        th = {}

        for index, row in enumerate(coins_arr):
            ev["x" + str(index)] = threading.Event()
            th["x" + str(index)] = threading.Thread(target=updateLinkHistory, args=(row,))
            th["x" + str(index)].start()

        ev["x1"].set()
        for i, t in enumerate(th):
            th["x" + str(i)].join()


    except Exception as e:
        d('ERROR')
        print('>>>:', e)


    conn.close()



def updateLinkHistory(coins_arr):

    d('NEW THREAD')

    # print(coins_arr)
    # exit()

    conn = pymysql.connect(host=dbConf['host'], port=dbConf['port'], user=dbConf['user'], passwd=dbConf['passwd'], db=dbConf['name'], charset=dbConf['charset'])
    cur = conn.cursor()

    for row in coins_arr:

        test_arr = False
        try:

            coin_id = row[0]
            coin_name = row[1]
            data = updateCurrentCoin(coin_name)

            d(coin_name)

            query = ([
                coin_id,
                data['changes_1h'],
                data['changes_24h'],
                data['changes_7d'],
                data['volume24_usd'],
                data['volume24_btc'],
                data['price_usd'],
                data['price_btc'],
                data['capital_usd'],
                data['capital_btc'],
                data['suply_number'],
                timestamp()
            ])
            test_arr = query
            # print(query)
            # exit()
            cur.execute(
                "INSERT INTO `coins_history` (`id`,`coin_id`,`changes_1h`,`changes_24h`,`changes_7d`,`volume24_usd`,`volume24_btc`,`price_usd`,`price_btc`,`capital_usd`,`capital_btc`,`suply_number`,`created_at`) VALUES (Null,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                query)
            history_id = cur.lastrowid
            conn.commit()

            query = ([
                data['extra']['rank'],
                data['extra']['contract_address'],
                data['coinmarketcap'],
                data['coinmarketcap_id'],
                timestamp(),
                coin_id
            ])

            d('UPDATED: ' + data['name'])
            print(query)

            cur.execute(
                "UPDATE `coins` SET `rank` = %s, `contract_address` = %s, `coinmarketcap` = %s, `coinmarketcap_id` = %s, `updated_at` = %s WHERE `id` = '%s'",
                query)
            conn.commit()


        except Exception as e:
            d('ERROR')
            print('>>>:', e)
            print(test_arr)
            d('FINISH')

    conn.close()



def main():
    parsList()
    # updateLinks()
    # page('https://coinmarketcap.com/currencies/airswap/')
    # page('https://coinmarketcap.com/currencies/eos/')
    # page('https://coinmarketcap.com/es/currencies/comsa-eth/')


if __name__ == "__main__":
    main()
