<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'baseUrl' => 'http://localhost/',
            'hostInfo' => 'http://localhost/',
        ],
        'sitemap' => [
            'class' => '\zhelyabuzhsky\sitemap\components\Sitemap',
            'maxUrlsCountInFile' => 10000,
            'sitemapDirectory' => 'frontend/web',
            'optionalAttributes' => ['changefreq', 'lastmod', 'priority'],
            'optionalAttributes' => [],
            'maxFileSize' => '10M',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            // 'dsn' => 'mysql:host=localhost;dbname=yii2_test',
            'dsn' => 'mysql:host=localhost;dbname=bittrade;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock;',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',

            // Schema cache options (for production environment)
            //'enableSchemaCache' => true,
            //'schemaCacheDuration' => 60,
            //'schemaCache' => 'cache',
        ],
    ],
    'params' => $params,

];
