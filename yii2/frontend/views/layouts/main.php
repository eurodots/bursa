<?php

$react_css = '/reactjs/*.css';
$react_state = '';
$react_js = '/reactjs/*.js';

$reactFiles = glob('reactjs/' . '*.{js,css}', GLOB_BRACE);
foreach ($reactFiles as $file) {
    if (strpos($file, '.js') !== false) {
        $react_js = '/'.$file;
    } elseif (strpos($file, '.css') !== false) {
        $react_css = '/'.$file;
    }
}



/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use nirvana\jsonld\JsonLDHelper;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= Html::csrfMetaTags() ?>

    <meta data-react-helmet="true" charset="<?= Yii::$app->charset ?>"/>


    <?php
        if(isset($this->context->jsonld)) {
            JsonLDHelper::add($this->context->jsonld);
            JsonLDHelper::registerScripts();
        }
    ?>

    <?php $this->head() ?>

    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title data-react-helmet="true"><?= Html::encode($this->title) ?></title>
    <meta data-react-helmet="true" http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta data-react-helmet="true" name="viewport" content="width=device-width, initial-scale=1"/>
    <meta data-react-helmet="true" name="msapplication-tap-highlight" content="no"/>
    <meta data-react-helmet="true" name="mobile-web-app-capable" content="yes"/>
    <meta data-react-helmet="true" name="apple-mobile-web-app-capable" content="yes"/>
    <meta data-react-helmet="true" name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta data-react-helmet="true" name="apple-mobile-web-app-title" content="reactGo"/>

    <link rel="icon" type="image/png" href="/favicon.png" />


    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" href="<?=$react_css?>" />

    <script>
        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
        ga('create', '<?=\Yii::$app->params['googleAnalytics']?>', 'auto');
        ga('send', 'pageview');
    </script>
    <script async src='https://www.google-analytics.com/analytics.js'></script>


    <style media="screen" type="text/css">

      [data-el="preload"] {
        align-items: center;
        background-color: white;
        display: flex;
        height: 100vh;
        justify-content: center;
        width: 100vw;
      }

      [data-el="preload"] > div {
        -webkit-animation: sk-scaleout 1.0s infinite ease-in-out;
        animation: sk-scaleout 1.0s infinite ease-in-out;
        background-color: rgb(63, 81, 181);
        border-radius: 100%;
        height: 6em;
        width: 6em;
      }

      @keyframes sk-scaleout {
        0% {
          -webkit-transform: scale(0);
          transform: scale(0);
        }
        100% {
          -webkit-transform: scale(1.0);
          opacity: 0;
          transform: scale(1.0);
        }
      }
    </style>

</head>
<body>


<div id="root">

<?php $this->beginBody() ?>

    <div data-el="preload"><div></div></div>

    <div class="wrap" style="padding: 50px">
        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        $menuItems = [
            // ['label' => 'Home', 'url' => ['/site/index']],
            // ['label' => 'About', 'url' => ['/site/about']],
            // ['label' => 'Contact', 'url' => ['/site/contact']],
        ];
        if (Yii::$app->user->isGuest) {
            // $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
            // $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
        } else {
            $menuItems[] = '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
        NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>

            <h1><?=$this->title?></h1>
            <p><a href="/">Bursadex</a></p>
            <div>
                <?= $content ?>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</div>

<?php /*
*/ ?>

<?=$react_state ?>
<script async type="text/javascript" charset="utf-8" src=<?=$react_js?>></script>

</body>
</html>
<?php $this->endPage() ?>
