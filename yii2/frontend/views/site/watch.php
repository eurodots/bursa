<?php
use yii\helpers\Url;
$currentUrl = Yii::$app->request->absoluteUrl;

$projectName = Yii::$app->name;
$title = $model->title;
$channel_name = $model->channel->name;
$channel_url = 'https://www.youtube.com/user/'.$model->channel->identity;
$video_url = 'https://www.youtube.com/watch?v='.$model->identity;
$video_embed = 'https://www.youtube.com/embed/'.$model->identity;
$description = $model->description;
$language = $model->language->iso;
$video_views = $model->stat->show;
$video_likes = $model->stat->likes;
$video_duration = $model->stat->duration;
// $video_tags = $model->tag;
// $video_categories = $model->categories;
// print_r($model);
// exit();
// "keywords" => "[skate,skateboarding,sci-fi,vfx]",

$video_upload = $model->stat->upload;
$video_upload = new DateTime("@$video_upload");
$video_upload = $video_upload->format('Y-m-d');

$video_created = $model->created_at;
$video_created = new DateTime("@$video_created");
$video_created = $video_created->format('Y-m-d');

$video_thumbnail = "https://img.youtube.com/vi/".$model->identity."/0.jpg";

$meta_description = preg_replace( "/\r|\n/", "", $description );
$meta_description = mb_substr($meta_description, 0, 200).'...';

$description_subtitles = '';
if(isset($model->subtitlesdesc->text)) {
    $description_subtitles = $model->subtitlesdesc->text;
}

// print_r($video_upload);
// // // print_r($model);
// exit();

/* @var $this yii\web\View */
$this->title = $title.' | '.$projectName;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $meta_description
]);

$this->registerMetaTag([
    'http-equiv' => 'content-language',
    'content' => $language
]);

// print_r($model->language->iso);

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $title.' | '.$projectName
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => $meta_description
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'video.movie'
]);
$this->registerMetaTag([
    'property' => 'og:video',
    'content' => $video_embed
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => $video_thumbnail
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => $currentUrl
]);
$this->registerMetaTag([
    'property' => 'fb:app_id',
    'content' => 166812790589229
]);



// <meta property="og:title" content="The Rock" />
// <meta property="og:type" content="video.movie" />
// <meta property="og:url" content="http://www.imdb.com/title/tt0117500/" />
// <meta property="og:image" content="http://ia.media-imdb.com/images/rock.jpg" />
$this->context->jsonld = [
    "@type" => "http://schema.org/VideoObject",
    "@id" => $video_embed,

    "http://schema.org/mainEntityOfPage" => (object)[
        "@type" => "http://schema.org/VideoObject",
        "@id" => $video_embed,
    ],

    "http://schema.org/name" => $title,
    "http://schema.org/uploadDate" => $video_upload,
    "http://schema.org/datePublished" => $video_created,
    "http://schema.org/thumbnailUrl" => $video_thumbnail,
    "http://schema.org/description" => $description_subtitles,
    "http://schema.org/embedUrl" => $video_embed,
    "http://schema.org/duration" => $video_duration,

    "http://schema.org/author" => (object)[
        "@type" => "http://schema.org/Person",
        "http://schema.org/name" => $channel_name,
        "http://schema.org/url" => $channel_url,
        // "http://schema.org/sameAs" => [
        //     "https://www.instagram.com/kitharingtonn/",
        // ]
    ],
    "http://schema.org/interactionStatistic" => [
        (object)[
            "@type" => "InteractionCounter",
            "http://schema.org/interactionService" => (object)[
                "@type" => "Website",
                "http://schema.org/name" => "YouTube",
                "@id" => $video_url,
            ],
            "http://schema.org/interactionType" => "http://schema.org/LikeAction",
            "http://schema.org/userInteractionCount" => $video_likes
        ],
        (object)[
            "@type" => "InteractionCounter",
            "http://schema.org/interactionType" => "http://schema.org/WatchAction",
            "http://schema.org/userInteractionCount" => $video_views
        ],

    ],

];


?>


<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$model->identity?>" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
<p><?=nl2br($description, false)?></p>
<a href="<?=$video_url?>" rel="nofollow">More about «<?=$title?>»...</a>
<p>
    <?=nl2br($description_subtitles, false)?>
</p>
