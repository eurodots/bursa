<?php

namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use api\models\Signup;
use api\models\User;
use api\models\UserLanguage;
use api\models\Token;

// use yii\db\Expression;


// http://demohost.com:8888/signup?email=maaffa@fdil.cfom&password=123&language=en

header('Access-Control-Allow-Origin: *');

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */

    public function actionIndex($action=false)
    {

        // $this->layout = 'json';
        echo "<pre>";
        print_r($_SERVER['REQUEST_METHOD']);
        echo "</pre>";
        // return $this->render('index', [
        //     'videos' => $json,
        // ]);
    }


    public function actionSignup()
    {

        if(
            isset($_GET['name']) &&
            isset($_GET['email']) &&
            isset($_GET['password']) &&
            isset($_GET['language'])
        ) {
            $name = (string)$_GET['name'];
            $email = (string)$_GET['email'];
            $password = (string)$_GET['password'];
            $language = (string)$_GET['language'];
            $language = substr($language, 0, 2);

            $language_id = false;

            $respond = (object)[];

            if(strlen($name) < 3 || strlen($name) > 30) {
                $respond = [
                    "status" => 301,
                    "message" => "Name must be between 3 and 30 characters"
                ];
                print_r( json_encode($respond) );
                exit();
            }

            $validator = new \yii\validators\EmailValidator();
            if (!$validator->validate($email, $error)) {
                $respond = [
                    "status" => 302,
                    "message" => "Email isn't valid" //$error
                ];
                print_r( json_encode($respond) );
                exit();
            }

            if(strlen($password) < 6 || strlen($password) > 30) {
                $respond = [
                    "status" => 303,
                    "message" => "Password must be between 6 and 30 characters"
                ];
                print_r( json_encode($respond) );
                exit();
            }

            $query = UserLanguage::find()->where(['iso' => $language])->one();
            $language_id = false;
            if(count($query) == 0) {
                $model = new UserLanguage();
                $model->iso = $language;
                $model->name = $language;
                $model->save();

                $language_id = $model->id;
            } else {
                $language_id = $query->id;
            }
            // print_r($query->id);


            $query = User::find()->where(['email' => $email])->one();
            $user_id = false;
            if(count($query) == 0) {
                $model = new Signup();
                $model->name = $name;
                $model->email = $email;
                $model->password = \Yii::$app->security->generatePasswordHash($password);;
                $model->language_id = $language_id;
                $model->status = 0;
                $model->created_at = time();
                $model->updated_at = time();
                $model->save();

                $user_id = $model->id;

                // $model = new Signup();
                // $json = $model->auth($user_id)->token;

                $respond = [
                    "status" => 200,
                    "token" => $model->auth($user_id)->token,
                ];
            } else {
                $user_id = $query->id;
                $respond = [
                    "status" => 302,
                    "message" => "User exist!"
                ];
                // echo ;
                // exit();
            }
            // print_r($query->id);

            print_r( json_encode($respond) );


        }


        // $model = new LoginForm();
        // $model->name = 'ok name';
        // $model->email = 'mail@mail.com';
        // $model->password = 'ok password';
        // $model->save();


        // $this->layout = 'json';
        // echo "<pre>";
        // print_r($_SERVER['REQUEST_METHOD']);
        // echo "</pre>";
        // return $this->render('index', [
        //     'videos' => $json,
        // ]);
    }

    public function actionLogin() {
        if(
            isset($_GET['email']) &&
            isset($_GET['password'])
        ) {
            $email = $_GET['email'];
            $password = $_GET['password'];

            $respond = (object)[];

            $validator = new \yii\validators\EmailValidator();
            if (!$validator->validate($email, $error)) {
                $respond = [
                    "status" => 302,
                    "message" => "Email isn't valid" //$error
                ];
                print_r( json_encode($respond) );
                exit();
            }

            if(strlen($password) < 6 || strlen($password) > 30) {
                $respond = [
                    "status" => 303,
                    "message" => "Password must be between 6 and 30 characters"
                ];
                print_r( json_encode($respond) );
                exit();
            }

            $query = User::find()->where(['email' => $email])->one();
            $user_id = $query ? $query->id : false;
            if($user_id == false) {
                $respond = [
                    "status" => 302,
                    "message" => "Email not found"
                ];
                print_r( json_encode($respond) );
                exit();
            }
            $passwordCheck = Yii::$app->security->validatePassword($password, $query->password);
            if($passwordCheck == 1) {
                $model = new Signup();
                $respond = [
                    "status" => 200,
                    "token" => $model->auth($user_id)->token
                ];
                print_r( json_encode($respond) );
                exit();
            } else {
                $respond = [
                    "status" => 303,
                    "message" => "Incorrect password"
                ];
                print_r( json_encode($respond) );
                exit();
            }
            // print_r();

            // echo "ok";
        } else {
            $respond = [
                "status" => 400,
                "message" => "Incorrect request"
            ];
            print_r( json_encode($respond) );
            exit();
        }
    }



    public function actionToken($token=false) {
        if($token) {
            // $query = Token::->getUser
            // $model = new Token;

            print_r(Token::getUser($token));
            // $query = Token::find()->with(['user'])->where(['token' => $token])->one();
            // if(count($query) > 0) {
            //     if( $query->expired_at < time()) {
            //         echo "Token expired";
            //     } else {
            //         echo "Token success\n";
            //         echo "User id: ".$query->user_id;
            //     }
            // } else {
            //     echo "Token not found";
            // }
        }
    }
}
