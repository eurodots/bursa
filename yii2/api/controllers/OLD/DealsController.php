<?php

namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

// use api\models\Signup;
// use api\models\Users;
// use api\models\Language;
// use api\models\Token;

// use yii\db\Expression;
use api\models\Token;
use api\models\OrderDeal;
use api\models\OrderStrategy;
use api\models\Pair;
use api\models\Stock;
use api\models\StockPair;
use api\models\StockAccount;
use api\models\StockAccountBalance;
use api\models\StatCoinmarketcap;


use Binance\API;
use liqui\api\LiquiAPITrading;



header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");



class DealsController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }


    /**
     * Displays JSON videos.
     *
     * @return string
     */

    public function actionIndex($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {
                $query = OrderDeal::find()->with(['pair','account','strategy'])->where(['user_id' => $user_id])->asArray()->all();
                // $array = [];
                // foreach ($query as $key => $item) {
                //     $array[] = (array) $item;
                // }
                // $array = (array) $query;
                // print_r($query);
                $result = array (
                    "status" => 200,
                    "results" => $query
                );
                print_r($result);
                exit();
                print_r( json_encode($result) );
                exit();
            }
            // print_r($user_id);


        }
    }

    public function placeOrderByApi($type, $user_id, $arr) {


        $account_id = $arr['account_id'];
        $balance_id = $arr['balance_id'];
        $volume = number_format($arr['volume'], 8);
        // $price = $arr['price'] * 10000;
        $price = number_format($arr['price'], 8);
        $coin_from = $arr['coin_from'];
        $coin_to = $arr['coin_to'];



        // GET API KEYS FOR AN ACCOUNT
        $modelAccount = StockAccount::find()->where(['id' => $account_id, 'user_id' => $user_id])->asArray()->one();
        $api = [];
        if(strlen($modelAccount['stock_api']) > 0) {
            $api_string = explode(PHP_EOL,$modelAccount['stock_api']);
            $stock_api = [];
            foreach ($api_string as $api) {
                $string_split = explode('=',$api);
                $stock_api[$string_split[0]] = $string_split[1];
            }
            $api = [
                "account_id" => $modelAccount['id'],
                "user_id" => $modelAccount['user_id'],
                "stock_id" => $modelAccount['stock_id'],
                "stock_api" => $stock_api,
            ];
        }


        if($type == 'sell' || $type == 'buy') {

            $place_type = $type == 'sell' ? 0 : 1;

                // BINANCE
                if($api['stock_id']==2) {

                    $apiKey = isset($api['stock_api']['apiKey']) ? $api['stock_api']['apiKey'] : false;
                    $apiSecret = isset($api['stock_api']['apiSecret']) ? $api['stock_api']['apiSecret'] : false;

                    if($apiKey && $apiSecret) {
                        $binance = new API($apiKey,$apiSecret);
                        $pair = $coin_from.$coin_to;


                        // $demo = [
                        //     "place_type" => $type,
                        //     "pair" => $pair,
                        //     "volume" => $volume,
                        //     "price" => $price,
                        // ];
                        //
                        // print_r($demo);
                        // exit();
                        if($place_type) {
                            $order = $binance->buy($pair, $volume, $price);
                        } else {
                            $order = $binance->sell($pair, $volume, $price);
                        }

                        if(isset($order['code'])) {
                            print_r($order);
                            exit();
                        } else {
                            return $order['orderId'];
                        }

                    }

                }
        }

        if($type == 'cancel' && strlen($arr['api_order_id']) > 0) {
            $api_order_id = $arr['api_order_id'];

            // BINANCE
            if($api['stock_id']==2) {
                $apiKey = isset($api['stock_api']['apiKey']) ? $api['stock_api']['apiKey'] : false;
                $apiSecret = isset($api['stock_api']['apiSecret']) ? $api['stock_api']['apiSecret'] : false;

                if($apiKey && $apiSecret) {
                    $binance = new API($apiKey,$apiSecret);
                    $pair = $coin_from.$coin_to;
                    $order = $binance->cancel($pair, $api_order_id);

                    if($order) { //success
                        return true;
                    } else {
                        print_r($order);
                        exit();
                    }
                }
            }
        }


        return false;
        exit();
    }



    public function actionDeals_save($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);


            $array = file_get_contents("php://input");
            $fp = fopen('array.json', 'w');
            fwrite($fp, print_r($array, TRUE));
            fclose($fp);

            $json = preg_replace( "/\p{Cc}*$/u", "", $array);
            $deal = json_decode($json, TRUE);


            if(isset($deal)) {


                // print_r($current_pair);
                // exit();

                // ORDER API!!!
                $api_order_id = '';
                if($deal['form_id'] == false) { //PLACE ORDER

                    $arr = [
                        "account_id" => $deal['account_id'],
                        "balance_id" => $deal['balance_id'],
                        "volume" => $deal['volume'],
                        "price" => $deal['price'],
                        "coin_from" => $deal['current_pair']['coin_from'],
                        "coin_to" => $deal['current_pair']['coin_to'],
                    ];

                    if($deal['status_id'] == 0) {
                        if($deal['type'] == 0) { //SELL
                            $api_order_id = $this->placeOrderByApi('sell', $user_id, $arr);
                        } elseif($deal['type'] == 1) { //BUY
                            $api_order_id = $this->placeOrderByApi('buy', $user_id, $arr);
                        }
                    }


                } elseif($deal['status_id'] == 2) { //CANCEL ORDER

                    $model = OrderDeal::find()->where(['id' => $deal['form_id']])->with(['pair'])->asArray()->one();

                    // print_r($model);
                    // exit();
                    if(count($model) > 0) {
                        $arr = [
                            "account_id" => $model['stock_account_id'],
                            "balance_id" => $model['balance_id'],
                            "volume" => $model['volume'],
                            "price" => $model['price'],
                            "coin_from" => $model['pair']['coin_from'],
                            "coin_to" => $model['pair']['coin_to'],
                            "api_order_id" => $model['api_order_id'],
                        ];
                        // print_r($arr);
                        // exit();

                        $api_response = $this->placeOrderByApi('cancel', $user_id, $arr);
                        if(!$api_response) {
                            print_r('ERROR CANCEL ORDER');
                            exit();
                        }

                    }

                }




                // ORDER SAVE/UPDATE

                $modelPair = Pair::find()->where(['coin_from' => $deal['current_pair']['coin_from'], 'coin_to' => $deal['current_pair']['coin_to']])->asArray()->one();
                $pair_id = $modelPair ? $modelPair['id'] : false;


                $stocks = [];
                foreach ($deal['stocks'] as $stock) {
                    if($stock['active']) {
                        $stocks[] = $stock['id'];
                    }
                }
                $stocks = join(',', $stocks);



                if($deal['form_id']) { // UPDATE

                    // $profit_fix = $deal['profit_fix'];
                    // if($profit_fix == 0) {
                    //     $parentArr = $this->getDataByParentId($user_id, $deal['parent_id']);
                    //     print_r($parentArr);
                    //     exit();
                    // }


                    $model = OrderDeal::find()->where(['id' => $deal['form_id']])->one();
                    $model->profit_fix = $deal['profit_fix'];
                    $model->profit_fix_percent = $deal['profit_fix_percent'];
                    $model->profit_min = $deal['profit_min'];
                    $model->profit_max = $deal['profit_max'];
                    $model->stocks = $stocks;
                    $model->strategy_id = $deal['strategy_id'];
                    $model->note = $deal['note'];
                    $model->status = $deal['status_id'];
                    $model->updated_at = time();
                    $model->save();

                } else { // ADD

                    $model = new OrderDeal();
                    $model->user_id = $user_id;
                    $model->parent_id = $deal['parent_id'];
                    $model->stock_account_id = $deal['account_id'];
                    $model->api_order_id = $api_order_id;
                    $model->pair_id = $pair_id;
                    $model->balance_id = $deal['balance_id'];
                    $model->volume = $deal['volume'];
                    $model->price = $deal['price'];
                    $model->profit_fix = $deal['profit_fix'];
                    $model->profit_fix_percent = $deal['profit_fix_percent'];
                    $model->profit_min = $deal['profit_min'];
                    $model->profit_max = $deal['profit_max'];
                    $model->type = $deal['type'];
                    $model->stocks = $stocks;
                    $model->strategy_id = $deal['strategy_id'];
                    $model->note = $deal['note'];
                    $model->status = $deal['status_id'];
                    $model->updated_at = time();
                    $model->created_at = time();
                    $model->save();

                }


                $respond = [
                    "status" => 200,
                    "message" => "Deal saved"
                ];
                print_r( json_encode($respond) );
                exit();
            }


        }

        exit();
    }


    public function getApiOrder($deal_id=false) {

        if($deal_id) {
            $orders = json_decode(file_get_contents("json/apiorders.json"), true);
            $order_extract = false;

            foreach ($orders as $order) {
                if($order['deal']['id'] == $deal_id) {
                    $order_extract = $order;
                }
            }
            return $order_extract;
        }

        return false;
    }

    public function actionOrder_deals($token=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                $modelDeal = OrderDeal::find()->with(['pair','transaction','account','strategy','case'])->where(['user_id' => $user_id, 'status' => [0,1,2]])->asArray()->all();

                $arrDeal = [];
                $arrDealType = [
                    0 => "Sell",
                    1 => "Buy"
                ];
                // $arrDealStatus = [
                //     0 => "In progress",
                //     1 => "Complete",
                //     2 => "Canceled",
                //     3 => "Deleted",
                // ];
                foreach ($modelDeal as $key => $deal) {


                    $commission = $deal['account']['stock']['commission'];
                    $total = ($deal['volume'] * $deal['price']);
                    $fee = ($total/100) * $commission;
                    $total_with_fee = $deal['type'] ? $total + $fee : $total - $fee;
                    // $profit_min_fix = ($total_with_fee / 100) * $deal['profit_min'];
                    $pair_label = $deal['pair']['coin_from']."_".$deal['pair']['coin_to'];

                    $arrDeal[] = [
                        "id" => (int)$deal['id'],
                        "status" => dealStatuses($deal['status']),
                        // "status_id" => $deal['status'],
                        "pair" => $pair_label,
                        "volume" => $deal['volume'],
                        "volume_label" => $deal['pair']['coin_from'],
                        "price" => $deal['price'],
                        "price_label" => $deal['pair']['coin_to'],
                        // "profit" => $deal['profit']."%",
                        "total" => $total_with_fee,
                        "total_label" => $deal['pair']['coin_to'],
                        "type" => $arrDealType[$deal['type']],
                        // "type_id" => (int)$deal['type'],
                        // "strategy" => $deal['strategy']['name'],
                        // "strategy_id" => (int)$deal['strategy']['id'],
                        "profit" => [
                            "min" => $deal['profit_min'],
                            "max" => $deal['profit_max'],
                            "fix" => $deal['profit_fix'],
                            "status" => $this->getApiOrder($deal['id']),
                        ],
                        // "profit_label" => $deal['pair']['coin_to'],
                        // "note" => $deal['note'],
                        // "case_name" => $deal['case']['case']['name'],
                        // "case_note" => $deal['case']['case']['note'],
                        "api_order_id" => $deal['api_order_id'],
                        "status" => dealStatuses($deal['status']),
                        "created_at" => dateConverter($deal['created_at']),
                        "updated_at" => dateConverter($deal['updated_at']),
                        "account" => $deal['account']['name'],
                        "account_id" => (int)$deal['account']['id'],
                        "stock" => $deal['account']['stock']['name'],
                        "stock_commission" => ($deal['type'] ? "+" : "-").$commission."%",
                        // "transactions" => [],
                        // "note" => $deal['note'],
                    ];
                    // print_r($deal['account']);
                    // exit();
                    $arrDealId = count($arrDeal)-1;
                    $arrDealTransactionStatus = [
                        0 => "In progress",
                        1 => "Complete",
                        2 => "Deleted",
                    ];
                    foreach ($deal['transaction'] as $k => $transaction) {
                        $arrDeal[$arrDealId]['transactions'][] = [
                            "volume" => $transaction['volume'],
                            "coin" => $transaction['coin']['iso'],
                            "note" => $transaction['note'],
                            "status" => $arrDealTransactionStatus[$transaction['status']],
                            "account_from" => $transaction['account_from']['name']." (".$transaction['account_from']['stock']['name'].")",
                            "account_to" => $transaction['account_to']['name']." (".$transaction['account_to']['stock']['name'].")",
                        ];
                    }
                }


                $result = array (
                    "status" => 200,
                    "results" => $arrDeal,
                );
                // print_r($result);
                print_r( json_encode($result) );
                exit();


            }
        }
    }

    public function getCoinmarketcap($coin_label) {
        if($coin_label) {
            $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();
            $arr = [];
            foreach ($modelCoinmarketcap as $coinmarketcap) {
                if($coin_label == $coinmarketcap['coin_label']) {
                    $arr = $coinmarketcap;
                }
            }
            return $arr;
        }
        return false;
    }

    public function getDataByParentId($user_id=false, $id=false) {
        if($user_id && $id) {

            $modelDeal = OrderDeal::find($id)->where(['id' => $id, 'user_id' => $user_id])->with(['pair'])->asArray()->one();

            if(count($modelDeal)>0) {
                $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();

                $price_coin_from = $this->getCoinmarketcap($modelDeal['pair']['coin_from'])['price_usd'];
                $price_coin_to = $this->getCoinmarketcap($modelDeal['pair']['coin_to'])['price_usd'];

                // $currencyExchange = [
                //     "coin_from" => $this->getCoinmarketcap($modelDeal['pair']['coin_from'])['price_usd'],
                //     "coin_to" => $price_coin_to,
                // ];
                // print_r($currencyExchange);
                // exit();

                $total = $modelDeal['price'] * $modelDeal['volume'];
                $total_volume = false;
                $coin_label = false;

                if($modelDeal['type']) { //Buy
                    $total_volume = $modelDeal['volume'];
                    $coin_label = $modelDeal['pair']['coin_from'];
                    $total_usd = $price_coin_from * $total_volume;
                } else { //Sell
                    $total_volume = $total;
                    $coin_label = $modelDeal['pair']['coin_to'];
                    $total_usd = $price_coin_to * $total_volume;
                }



                $arr = [
                    "deal_id" => $modelDeal['id'],
                    "deal_type" => $modelDeal['type'],
                    "pair" => $modelDeal['pair'],
                    "coin_label" => $coin_label,
                    "volume" => $modelDeal['volume'],
                    "price" => $modelDeal['price'],
                    "total_volume" => $total_volume,
                    "total_usd" => $total_usd,
                    // "_meta" => [
                        // "coin" => $modelDeal['pair']['coin_from'],
                        // "total_usd" => $total * $currencyExchange,
                    // ],

                    // "" => $modelDeal[''],
                    // "" => $modelDeal[''],

                ];
                return $arr;
            }

            return false;
        }
    }

    public function actionOrder_deals_form($token=false, $id=false)
    {
        if($token) {
            $user_id = Token::getUser($token);
            if($user_id) {

                // if($id) {
                //     $modelDeal = OrderDeal::find()->with(['pair','transaction','account','strategy','case'])->where(['id' => $id, 'user_id' => $user_id])->asArray()->all();
                // } else {
                //     $modelDeal = OrderDeal::find()->with(['pair','transaction','account','strategy','case'])->where(['user_id' => $user_id])->asArray()->all();
                // }

                $globalCoins = \Yii::$app->params['globalCoins'];
                $modelDeal = OrderDeal::find()->with(['pair','transaction','account','strategy','case'])->where(['id' => $id, 'user_id' => $user_id])->asArray()->one();

                // print_r();
                // exit();

                // $deal_coin_from = $modelDeal['pair']['coin_from'];
                // $modelBalance = StockAccountBalance::find()->where(['user_id' => ])->asArray()->all();
                // $modelAccount = StockAccount::find()->where(['user_id' => $user_id])->with(['wallet'])->asArray()->all();
                // $walletArr = [];
                // foreach ($modelAccount['wallet'] as $wallet) {
                //     print_r($wallet);
                //     exit();
                //     // if($wallet['coin_label'] == $deal_coin_from) {
                //     //     $walletArr = $wallet;
                //     // }
                // }
                // print_r($deal_coin_from);
                // exit();

                $balance_id = $modelDeal['balance_id'];
                $parentData = false;
                $parent_id = isset($_GET['parent_id']) ? $_GET['parent_id'] : false;
                if($parent_id) {
                    $parentData = $this->getDataByParentId($user_id, $parent_id);
                }


                $arrDeal = false;
                $arrDealType = [
                    0 => "Ask",
                    1 => "Bid"
                ];

                $arrCurrentStocks = [];
                if(count($modelDeal) > 0) {

                    // $commission = $deal['account']['stock']['commission'];
                    // $total = ($deal['volume'] * $deal['price']);
                    // $fee = ($total/100) * $commission;
                    // $total_with_fee = $deal['type'] ? $total + $fee : $total - $fee;
                    // $profit_min_fix = ($total_with_fee / 100) * $deal['profit_min'];
                    // $pair_label = $deal['pair']['coin_from']."_".$deal['pair']['coin_to'];
                    $arrCurrentStocks = explode(",", $modelDeal['stocks']);

                    $api_order_id = $modelDeal['api_order_id'];
                    $api_order_exist = strlen($api_order_id) > 0 ? true : false;


                    $parentData = $modelDeal['parent_id'] ? $this->getDataByParentId($user_id, $modelDeal['parent_id']) : false;


                    $arrDeal = [
                        "deal_id" => $modelDeal['id'],
                        "balance" => [
                            "id" => $modelDeal['balance_id'],
                        ],
                        "volume" => $modelDeal['volume'],
                        "price" => $modelDeal['price'],
                        "profit_fix" => $modelDeal['profit_fix'],
                        "profit_fix_percent" => $modelDeal['profit_fix_percent'],
                        "profit_min" => $modelDeal['profit_min'],
                        "profit_max" => $modelDeal['profit_max'],
                        "coin_to" => $modelDeal['pair']['coin_to'],
                        "strategy_id" => $modelDeal['strategy_id'],
                        "status" => dealStatuses($modelDeal['status']),
                        "type" => [
                            "code" => (int)$modelDeal['type'],
                            "label" => $arrDealType[$modelDeal['type']],
                        ],
                        "note" => $modelDeal['note'],
                        "api_order" => [
                            "exist" => $api_order_exist,
                        ],
                        "created_at" => dateConverter($modelDeal['created_at']),
                        "updated_at" => dateConverter($modelDeal['updated_at']),
                    ];
                    $arrDealId = count($arrDeal)-1;
                    $arrDealTransactionStatus = [
                        0 => "In progress",
                        1 => "Complete",
                        2 => "Deleted",
                    ];
                    foreach ($modelDeal['transaction'] as $k => $transaction) {
                        $arrDeal[$arrDealId]['transactions'][] = [
                            "volume" => $transaction['volume'],
                            "coin" => $transaction['coin']['iso'],
                            "note" => $transaction['note'],
                            "status" => $arrDealTransactionStatus[$transaction['status']],
                            "account_from" => $transaction['account_from']['name']." (".$transaction['account_from']['stock']['name'].")",
                            "account_to" => $transaction['account_to']['name']." (".$transaction['account_to']['stock']['name'].")",
                        ];
                    }
                }



                $modelStocks = Stock::find()->with(['pairs_whitelist'])->asArray()->all();
                $arrStocks = [];
                foreach ($modelStocks as $stock) {
                    $active = in_array($stock['id'], $arrCurrentStocks) ? true : false;

                    $arrStocks[] = [
                        "id" => $stock['id'],
                        "name" => $stock['name'],
                        "price" => 0,
                        "first_order_price" => 0, //For reactjs
                        "active" => $active, //For reactjs
                    ];
                }
                usort($arrStocks,function($first,$second){
                    return $first['name'] > $second['name'];
                });


                $modelAccount = StockAccount::find()->where(['user_id' => $user_id])->with(['stock','balance'])->asArray()->all();

                $arrAccounts = [];

                foreach ($modelAccount as $account) {

                    $apiExist = count($account['stock_api']) > 0 ? true : false;




                    $stock_name = $account['stock']['name'];
                    $arrBalance = [];
                    foreach ($account['balance'] as $balance) {
                        $price_usd = $balance['available'] * $balance['coinmarketcap']['price_usd'];

                        if($price_usd > 1 || $balance['id'] == $balance_id) {

                            // $coins_to = array_diff($stockCoinsTo, array($balance['coin_label']));

                            // print_r($globalCoins);
                            // exit();
                            // $coin_type = 'coin_from';
                            // print_r($balance['coin_label']);
                            // exit();
                            foreach ($globalCoins as $coin) {
                                if($coin == $balance['coin_label']) {
                                    $coin_type = 'coin_to';
                                }
                            }



                            // GET ALL PAIRS FOR CURRENT STOCK AND WALLET COIN
                            $coin_pairs = [];
                            $coin_label = $balance['coin_label'];
                            $stock_id = $account['stock']['id'];
                            $modelWhitelist = StockPair::find()->where(['stock_id' => $stock_id, 'whitelist' => 1])->with(['pair_coin'])->asArray()->all();
                            foreach ($modelWhitelist as $pair) {
                                $pair_id = $pair['pair_id'];
                                $coin_from = $pair['pair_coin']['coin_from']['iso'];
                                $coin_to = $pair['pair_coin']['coin_to']['iso'];
                                if($coin_from == $coin_label && $coin_to && $coin_to != $coin_label) {
                                    $coin_pairs[] = [
                                        "pair" => [
                                            "coin_from" => $coin_from,
                                            "coin_to" => $coin_to,
                                            "direction" => 0,
                                        ],
                                        "coin" => $coin_to,
                                        "coinmarketcap" => $this->getCoinmarketcap($coin_to),
                                    ];
                                }
                                if($coin_to == $coin_label && $coin_from && $coin_from != $coin_label) {
                                    $coin_pairs[] = [
                                        "pair" => [
                                            "coin_from" => $coin_from,
                                            "coin_to" => $coin_to,
                                            "direction" => 1,
                                        ],
                                        "coin" => $coin_from,
                                        "coinmarketcap" => $this->getCoinmarketcap($coin_from),
                                    ];
                                }
                            }
                            usort($coin_pairs,function($first,$second){
                                return $first['coin'] > $second['coin'];
                            });





                            $arrBalance[] = [
                                "account_title" => $account['stock']['name'].": #".$account['id']." ".$account['name'],
                                "title" => $balance['coin_label']." — $".round($price_usd),
                                "balance_id" => $balance['id'],
                                "coin_label" => $balance['coin_label'],
                                // "coin_type" => $coin_type,
                                "available" => $balance['available'],
                                "updated_at" => $balance['updated_at'],
                                "usd" => [
                                    "price" => $balance['coinmarketcap']['price_usd'],
                                    "total" => $price_usd,
                                ],
                                "account_id" => $account['id'],
                                "stock_id" => $account['stock']['id'],
                                "stock_commission" => $account['stock']['commission'],
                                "pairs" => $coin_pairs,
                                "stock_api" => $apiExist,
                            ];
                        }
                    }
                    usort($arrBalance,function($first,$second){
                        return $first['usd']['total'] < $second['usd']['total'];
                    });




                    $arrAccounts[] = [
                        "title" => $account['stock']['name'].": #".$account['id']." ".$account['name'],
                        "stock" => [
                            "id" => $account['stock']['id'],
                            "name" => $account['stock']['name'],
                            "commission" => $account['stock']['commission'],
                        ],
                        "account" => [
                            "id" => $account['id'],
                            "name" => $account['name'],
                            "stock_api" => $apiExist,
                        ],
                        "balance" => $arrBalance,
                    ];

                }



                $modelStrategy = OrderStrategy::find()->asArray()->all();
                $arrStrategy = [];
                foreach ($modelStrategy as $key => $strategy) {
                    $arrStrategy[] = [
                        "id" => (int)$strategy['id'],
                        "label" => $strategy['name'],
                    ];
                }

                // $modelAccount = StockAccount::find()->where(['id' => $account,'user_id' => $user_id])->with(['stock'])->asArray()->one();
                // $stockId = $modelAccount['stock']['id'];


                $globalCoinsArr = [];
                $modelCoinmarketcap = StatCoinmarketcap::find()->asArray()->all();
                foreach ($globalCoins as $coin) {
                    foreach ($modelCoinmarketcap as $coinmarketcap) {
                        if($coin == $coinmarketcap['coin_label']) {
                            $globalCoinsArr[] = [
                                "label" => $coin,
                                "price_usd" => $coinmarketcap['price_usd'],
                            ];
                        }
                    }
                }


                $result = array (
                    "status" => 200,
                    "parent" => $parentData,
                    "form" => $arrDeal,
                    "accounts" => $arrAccounts,
                    "stocks" => $arrStocks,
                    "statuses" => dealStatuses(),
                    "strategies" => $arrStrategy,
                    "globalCoins" => $globalCoinsArr,

                );
                // print_r($result);
                print_r( json_encode($result) );
                exit();


            }
        }
    }

}


function dealStatuses($id=-1) {

    $arrDealStatus = [
        [
            "code" => 0,
            "label" => "In progress",
        ],
        [
            "code" => 1,
            "label" => "Completed",
        ],
        [
            "code" => 2,
            "label" => "Canceled",
        ],
        [
            "code" => 3,
            "label" => "Deleted",
        ]
    ];



    if($id >= 0) {
        foreach ($arrDealStatus as $status) {

            if($status['code'] == $id) {

                return $status;
            }
        }
    } else {
        return $arrDealStatus;
    }

    return false;
}

function getHomeUrl() {
    $hostInfo = Yii::$app->request->hostInfo;
    $baseUrl = Yii::$app->request->baseUrl;
    return $hostInfo.$baseUrl;
}

function dateConverter($epoch) {
    if($epoch) {
        $date = new \DateTime("@$epoch");
        $date = $date->format('Y-m-d H:i:s GMT');
        return $date;
    } else {
        return false;
    }
}

function time_elapsed_string($datetime, $full = false) {
    $now = new \DateTime;
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
