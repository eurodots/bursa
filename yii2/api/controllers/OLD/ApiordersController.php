<?php

namespace api\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

use api\models\Coin;
use api\models\Stock;
use api\models\StockAccount;
use api\models\StockAccountBalance;
use api\models\StockOrder;
use api\models\OrderDeal;



header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");


class ApiordersController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */

     public function actionIndex()
     {

         // $modelDeals = OrderDeal::find()->with(['pair','account','strategy'])->where(['user_id' => $user_id])->asArray()->all();
         $modelDeals = OrderDeal::find()->where(['status' => 0])->asArray()->all();
         $date = \Yii::$app->params['lastOrdersDate'];

         // print_r($modelStocks);
         // exit();

         $arrStockDeals = [];

         foreach ($modelDeals as $deal) {
             $stocks_id = explode(',', $deal['stocks']);
             $modelStocks = Stock::find()->where(['id' => $stocks_id])->asArray()->all();

             // print_r($deal);
             // exit();


             // MATCH WITH STOCK'S ORDERS
             $stock_orders = [];
             $counter_min = 0;
             $counter_max = 0;
             $mediaArr = [];
             $someLastDate = false;

             foreach ($stocks_id as $stock_id) {
                 $modelOrders = StockOrder::find()->with(['stock'])->where(['>', 'created_at', $date])->andWhere(['stock_id' => $stock_id, 'pair_id' => $deal['pair_id']])->asArray()->all();

                 if($deal['type']) { //Buy
                     usort($modelOrders,function($first,$second){
                         return $first['price'] < $second['price'];
                     });
                 } else { //Sell
                     usort($modelOrders,function($first,$second){
                         return $first['price'] > $second['price'];
                     });
                 }



                 if(count($modelOrders) > 0) {

                     $someLastDate = $modelOrders[0]['created_at'];
                     $price_stock = $modelOrders[0]['price'];

                     $price_deal = $deal['price'];
                     $profit_min = $deal['profit_min'];
                     $profit_max = $deal['profit_max'];

                     $percent = (($price_deal - $price_stock) / (($price_deal + $price_stock) / 2)) * 100;

                     $mediaArr[] = $percent;

                     if($percent && $percent < $profit_min) {
                         $counter_min++;
                     }
                     if($percent && $percent > $profit_max) {
                         $counter_max++;
                     }


                     $stock_orders[] = [
                         "percent" => $percent,
                         "first_order" => $modelOrders[0],
                     ];

                     // print_r([
                     //     "percent" => $percent,
                     //     "price_deal" => $price_deal,
                     //     "price_stock" => $price_stock,
                     //     "profit_min" => $profit_min,
                     //     "profit_max" => $profit_max,
                     //     // "deal" => $deal,
                     // ]);
                 } else {
                     // print_r([
                     //     "msg" => 'Nothing',
                     //     "price_stock" => $price_stock,
                     //     // "deal" => $deal,
                     // ]);
                 }

             }

             $stock_counter = count($stock_orders);
             $data_label = [];
             $difference = median($mediaArr);

             if($stock_counter == $counter_min) {
                 $data_label = "Small";
                 $difference = $difference - $deal['profit_min'];
             } elseif($stock_counter == $counter_max) {
                 $data_label = "Big";
                 $difference = $difference - $deal['profit_max'];
             } else {
                 $data_label = "Normal";
                 $difference = 0;
             }

             $rowColor = round($difference);
             if($rowColor > 0) {
                 if($rowColor > 3) {
                     $rowColor = 3;
                 }
                 $rowColor = "row-color-max-".$rowColor;
             }
             elseif($rowColor < 0) {
                 if($rowColor < -3) {
                     $rowColor = -3;
                 }
                 $rowColor = "row-color-min-".abs($rowColor);
             }
             elseif($rowColor == 0) { $rowColor = ""; }
             // print_r($rowColor);

             $arrDeal = [
                 "deal" => $deal,
                 "data" => [
                     "label" => $data_label,
                     "median" => median($mediaArr),
                     "median_difference" => round($difference, 2),
                     "row_color" => $rowColor,
                     "updated_at" => dateConverter($someLastDate),
                     "counter_min" => $counter_min,
                     "counter_max" => $counter_max,
                     "counter_stocks" => $stock_counter,
                 ],
                 "stock_orders" => $stock_orders,
             ];

             $arrStockDeals[] = $arrDeal;

         }



         $array = json_encode($arrStockDeals);
         $json = file_get_contents("php://input");
         $fp = fopen('json/apiorders.json', 'w');
         fwrite($fp, print_r($array, TRUE));
         fclose($fp);

         $response = json_decode(file_get_contents("json/apiorders.json"), true);
         print_r($response);


         exit();

     }


}


function median($array) {
    $count = count($array);
    sort($array);
    $mid = floor(($count-1)/2);
    // $avg = ($total_percent)?array_sum($total_percent)/$count:0;
    $median = ($array)?($array[$mid]+$array[$mid+1-$count%2])/2:0;

    return $median;
}

function dateConverter($epoch) {
    if($epoch) {
        $date = new \DateTime("@$epoch");
        $date = $date->format('Y-m-d H:i:s GMT');
        return $date;
    } else {
        return false;
    }
}
