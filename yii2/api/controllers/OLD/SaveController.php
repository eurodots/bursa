<?php

namespace api\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

// use api\models\Signup;
// use api\models\Users;
// use api\models\Language;
// use api\models\Token;

// use yii\db\Expression;
use api\models\Token;
use api\models\Playlists;
use api\models\PlaylistsItems;
use api\models\PlaylistsItemsParts;

// http://demohost.com:8888/signup?email=maaffa@fdil.cfom&password=123&language=en
// http://demohost.com:8888/signup?email=maaffa@adffdil.cfom&password=123&language=en
// http://demohost.com:8888/login?email=maaffa@fdil.cfom&password=123
// http://demohost.com:8888/token/KFwvIADhmDY7C06y3_77vSi9BgQMcc7J





header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT');


function getHomeUrl() {
    $hostInfo = Yii::$app->request->hostInfo;
    $baseUrl = Yii::$app->request->baseUrl;
    return $hostInfo.$baseUrl;
}


class SaveController extends Controller
{

    /**
     * @inheritdoc
     */
    // public function behaviors() {
    // }

    /**
     * @inheritdoc
     */
    // public function actions()
    // {
    // }

    /**
     * Displays JSON videos.
     *
     * @return string
     */

    public function actionIndex($token=false)
    {


        // $array = json_decode(file_get_contents("php://input"), true);
        // // // $object = (object) $array;
        // // $array = file_get_contents("php://input");
        // $fp = fopen('file.txt', 'w');
        // fwrite($fp, print_r($array, TRUE));
        // fclose($fp);


        // print_r($playlists->scalar);
        // print_r($array);

        // print_r( json_encode($object) );

        // function parsPlaylist($playlist=false) {
        //     foreach ($playlist as $key => $pl) {
        //
        //         // print_r($pl['id']);
        //     }
        //     // return
        // }


        if($token) {
            $user_id = Token::getUser($token);

            // $array = file_get_contents("php://input");


            $array = file_get_contents("php://input");
            $fp = fopen('array.json', 'w');
            fwrite($fp, print_r($array, TRUE));
            fclose($fp);

            if($array) {


            // $array = parse_str(file_get_contents("php://input"), $data);
            // print_r($array);
            // print_r($array);

            // var_dump($data);
            //
            //
            // // $decode = json_decode($array);
            // // parse_str($array, $data);
            // foreach ($data as $key => $arr) {
            //     print_r($arr);
            //     print_r('------------------------');
            // }
            //
            $playlists = json_decode($array, TRUE);

            // foreach ($playlists as $key => $playlist) {
            //     print_r($playlist);
            //     print_r('-------');
            // }
            // print_r($json);




            foreach ($playlists as $key => $playlist) {
                if(count($playlist)>0) {

                    $playlists_id = false;

                    if($playlist['manual']) {
                        // print_r($playlist);

                        $model = new Playlists();
                        $model->user_id = $user_id;
                        $model->name = strlen($playlist['name']) > 25 ? mb_substr($playlist['name'], 0, 30).'...' : $playlist['name'];
                        $model->hold = $playlist['hold'];
                        $model->active = $playlist['active'];
                        $model->shared = $playlist['shared'];
                        $model->deleted = 0;
                        $model->created_at = time();
                        $model->updated_at = time();
                        $model->save();

                        $playlists_id = $model->id;

                    } else {
                        $model = Playlists::find()->where(['id' => $playlist['id'], 'user_id' => $user_id, 'deleted' => 0])->one();
                        if(count($model)>0) {
                            $model->name = strlen($playlist['name']) > 25 ? mb_substr($playlist['name'], 0, 30).'...' : $playlist['name'];
                            $model->active = $playlist['active'];
                            $model->shared = $playlist['shared'];
                            $model->deleted = $playlist['deleted'];
                            $model->updated_at = time();
                            $model->save();

                            $playlists_id = $model->id;
                        }
                    }
                    if(count($playlist['items'])>0 && $playlists_id) {
                        foreach ($playlist['items'] as $i => $item) {

                            $item_id = false;
                            if($item['manual']) {
                                if($item['deleted'] == 0) {
                                    $model = new PlaylistsItems();
                                    $model->playlist_id = $playlists_id;
                                    $model->video_id = $item['video_id'];
                                    $model->video_identity = $item['video_identity'];
                                    $model->name = $item['name'];
                                    $model->status = 0;
                                    $model->deleted = 0;
                                    $model->created_at = time();
                                    $model->updated_at = time();
                                    $model->save();

                                    $item_id = $model->id;
                                }
                            } else {

                                $model = PlaylistsItems::findOne($item['id']);
                                if(count($model) > 0) {
                                    if($model->owner == $user_id) {
                                        $model->playlist_id = $playlists_id;
                                        // $model->name = $item['name'];
                                        $model->status = 0;
                                        $model->deleted = $item['deleted'];
                                        $model->updated_at = time();
                                        $model->save();

                                        $item_id = $model->id;

                                    }
                                }


                                // $model = PlaylistsItems::find($item['id'])->where(['video_id' => $video_id, 'deleted' => 0])->all();

                            }

                            if(count($item['parts'])>0 && $item_id) {

                                foreach ($item['parts'] as $key => $part) {
                                    if($part['manual']) {
                                        if($part['deleted'] == 0) {
                                            $model = new PlaylistsItemsParts();
                                            $model->item_id = $item_id;
                                            $model->start = $part['start'];
                                            $model->end = $part['end'];
                                            $model->name = $part['name'];
                                            $model->deleted = 0;
                                            $model->created_at = time();
                                            $model->updated_at = time();
                                            $model->save();
                                        }
                                    } else {
                                        $model = PlaylistsItemsParts::findOne($part['id']);
                                        if(count($model)>0 && $model->owner == $user_id) {
                                            $model->item_id = $item_id;
                                            $model->start = $part['start'];
                                            $model->end = $part['end'];
                                            $model->name = $part['name'];
                                            $model->deleted = $part['deleted'];
                                            $model->updated_at = time();
                                            $model->save();
                                        }

                                    }
                                }
                                // if($item['manual']) {
                                //     if($item['deleted'] == 0) {
                                //
                                //     }
                                // }
                                // $model = PlaylistsItemsParts::find()->where(['item_id' => $item_id])->all();
                                // if(count($model)>0) {
                                //     manual
                                // }
                                //  &&
                                // PlaylistsItemsParts::findOne($part_id)->owner == $user_id
                                //
                                // print_r($item['parts']);
                            }

                            // foreach ($item['parts'] as $p => $part) {
                            //     print_r($part);
                            // }
                        }
                    }
                }



                // if($playlist['manual']) {
                //     echo "1";
                // } else {
                //     echo "0";
                // }
            }


            }
/* */

        }

        exit();
    }


}
