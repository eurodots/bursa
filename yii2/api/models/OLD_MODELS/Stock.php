<?php
namespace api\models;
use yii\db\ActiveRecord;


class Stock extends ActiveRecord
{
    public static function tableName()
    {
        return 'stock';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }

    public function getWithdrawal()
    {
        return $this->hasMany(StockWithdrawal::className(), ['stock_id' => 'id'])->with(['coin']);
    }
    public function getOrder()
    {
        return $this->hasMany(StockOrder::className(), ['stock_id' => 'id']);
    }
    public function getPairs_whitelist()
    {
        return $this->hasMany(StockPair::className(), ['stock_id' => 'id'])->where(['whitelist' => 1])->with(['pair']);
    }

    public function getPairs_count()
    {
        return $this->hasMany(StockPair::className(), ['stock_id' => 'id']);
    }

    public function getData()
    {
        return $this->hasOne(StockData::className(), ['stock_id' => 'id']);
    }

}
