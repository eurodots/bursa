<?php
namespace api\models;
use yii\db\ActiveRecord;


class StockAccountBalance extends ActiveRecord
{
    public static function tableName()
    {
        return 'stock_account_balance';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }

    public function getCoinmarketcap()
    {
        return $this->hasOne(StatCoinmarketcap::className(), ['coin_id' => 'coin_id']);
    }

}
