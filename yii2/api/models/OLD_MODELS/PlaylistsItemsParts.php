<?php
namespace api\models;
use yii\db\ActiveRecord;


class PlaylistsItemsParts extends ActiveRecord
{
    public static function tableName()
    {
        return 'playlists_items_parts';
    }

    // public function attributeLabels() {
    //     return [
    //         'iso' => 'iso',
    //         'name' => 'name',
    //     ];
    // }
    //
    // public function rules() {
    //     return [
    //         [ ['iso', 'name'], 'required' ],
    //     ];
    // }

    public function getItem()
    {
        return $this->hasMany(PlaylistsItems::className(), ['id' => 'item_id']);
    }

    public function getOwner()
    {
        $playlist_id = PlaylistsItems::findOne($this->item_id)->playlist_id;
        $owner_id = Playlists::findOne($playlist_id)->user_id;
        return $owner_id;
    }

}
