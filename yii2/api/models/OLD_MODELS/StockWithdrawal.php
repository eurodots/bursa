<?php
namespace api\models;
use yii\db\ActiveRecord;


class StockWithdrawal extends ActiveRecord
{
    public static function tableName()
    {
        return 'stock_withdrawal';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }

    public function getStock()
    {
        return $this->hasOne(Stock::className(), ['id' => 'stock_id']);
    }

    public function getCoin()
    {
        return $this->hasOne(Coin::className(), ['id' => 'coin_id']);
    }

}
