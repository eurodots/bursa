<?php
namespace api\models;
use yii\db\ActiveRecord;


class UserLanguage extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_language';
    }

    public function attributeLabels() {
        return [
            'iso' => 'iso',
            'name' => 'name',
        ];
    }

    public function rules() {
        return [
            [ ['iso', 'name'], 'required' ],
        ];
    }

}
