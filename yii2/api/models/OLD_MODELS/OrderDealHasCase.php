<?php
namespace api\models;
use yii\db\ActiveRecord;


class OrderDealHasCase extends ActiveRecord
{
    public static function tableName()
    {
        return 'order_deal_has_case';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }

    public function getCase()
    {
        return $this->hasOne(OrderCase::className(), ['id' => 'case_id']);
    }

}
