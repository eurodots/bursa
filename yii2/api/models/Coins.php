<?php
namespace api\models;
use yii\db\ActiveRecord;


class Coins extends ActiveRecord
{
    public static function tableName()
    {
        return 'coins';
    }

    public function attributeLabels() {
        // return [
        //     'id' => 'id',
        // ];
    }

    public function rules() {
        return [
            // [ ['id'], 'required' ],
        ];
    }


    public function getLinks()
    {
        return $this->hasMany(CoinsLinks::className(), ['coin_id' => 'id']);
    }

    // public function getHistory()
    // {
    //     return $this->hasMany(CoinsHistory::className(), ['coin_id' => 'id']);
    // }

}
